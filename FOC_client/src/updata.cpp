#include "updata.h"
#include "ui_updata.h"
#include "winmain.h"
#include "math.h"
#include "QFileDialog"
#include "protool.h"
#include <QMessageBox>




QByteArray DataAllArray;
int fram_size=0;
int current_count=0;
int ack_ok=0; //返回成功
extern QSerialPort* lidar_serial;

updata::updata(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::updata)
{
    ui->setupUi(this);
    this->init();
}

updata::~updata()
{
    delete ui;
}


void updata::init(void)
{

    connect(&this->timer,&QTimer::timeout,this,&updata::SLOT_processTimer);
    connect(&this->send_timer,&QTimer::timeout,this,&updata::SLOT_processSendTimer);


}

void updata::on_pb_read_version_clicked()
{
    if(lidar_serial->isOpen()==false)
    {
        return;
    }
    set_computer_value(READ_VERSION_CMD,NULL,0);

}

void updata::setVersion(float ver)
{
    ui->lb_version->setText(QString("%1").arg(round(ver*100)/100.0f));


}


void updata::on_pb_scan_clicked()
{
    fileName = QFileDialog::getOpenFileName(this,
                                                     tr("固件选择"),
                                                     "../",
                                                     tr(
                                                        "BIN文件(*.bin)"));
    if(!fileName.isEmpty())
    {
       qDebug()<<"filename : "<<fileName;

       ui->le_addr->setText(fileName);
       this->LoadFrimware();
    }
}

void updata::on_pb_start_updata_clicked()
{
    if(ui->pb_start_updata->text()=="开始")
    {
       if(DataAllArray.isEmpty())
           return;
       update_p=DataAllArray.data();
       if(!lidar_serial->isOpen())
       {
           return;
       }
       ui->pb_start_updata->setText("等待单片机连接..");
       set_computer_value(RESET_CMD,NULL,0);
       this->timer.start(200); //启动定时器
    }
}

void updata::SLOT_processTimer(void)
{
    static int  count = 0;
    count++;
    qDebug()<<"总大小"<<fram_size;
    set_computer_value(SEND_FIRM_SIZE_CMD,&fram_size,4);
    qDebug()<<"发送bootloader连接请求";
    if(count>10)
    {
        ui->pb_start_updata->setText("开始");
        QMessageBox::warning(this,"错误","连接超时,请重试!!!",QMessageBox::Close);
        count=0;
        qDebug()<<"连接超时间";
        this->timer.stop();//定时器关闭
    }
}




void updata::SLOT_processSendTimer(void)
{
    if(lidar_serial->isOpen())
    {
        if(ack_ok==0)
        {
            return ;//如果没有接收到就返回
        }
        if(fram_size-current_count>=20)
        {
            current_count+=20;
            set_computer_value(SEND_FIRM_DATA_CMD,update_p,20);
            ack_ok = 0 ;

            update_p+=20; //字节偏移
        }
        else
        {
             set_computer_value(SEND_FIRM_DATA_CMD,update_p,fram_size-current_count);
             ack_ok = 0 ;
             current_count+=fram_size-current_count;
             qDebug()<<"发送完成";
             ui->pb_start_updata->setText("代码发送完毕,正在更新");

             this->send_timer.stop();
        }
        ui->progressBar->setValue(current_count);
        qDebug()<<"总共"<<fram_size<<"当前"<<current_count;
    }
}

void updata::enteredBootloader(void)
{
    this->timer.stop();
    ui->pb_start_updata->setText("刷写代码中");
    this->send_timer.start(10);
}



bool updata::LoadFrimware(void)
{
    QFile Frimware(fileName);
    if (!Frimware.open (QFile::ReadOnly))
    {
        qDebug()<<"打开失败";
        return 0;
    }
    DataAllArray=Frimware.readAll();
    fram_size=DataAllArray.size();
    ui->progressBar->setRange(0,fram_size);
    return 1;
}

void updata::updateFinish(void)
{
    ui->pb_start_updata->setText("开始");
    current_count=0;
}
