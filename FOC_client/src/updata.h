#ifndef UPDATA_H
#define UPDATA_H

#include <QDialog>
#include <QSerialPort>
#include <QTimer>

namespace Ui {
class updata;
}

class updata : public QDialog
{
    Q_OBJECT

public:
    explicit updata(QWidget *parent = nullptr);
    ~updata();

    void setVersion(float ver);
    QTimer timer;
    QTimer send_timer;
    QString fileName ;
    char * update_p;
    void init(void);
    void enteredBootloader(void);
    bool LoadFrimware(void);
    void updateFinish(void);
    Ui::updata *ui;
private slots:
    void on_pb_read_version_clicked();
    void SLOT_processTimer(void);

    void on_pb_scan_clicked();

    void on_pb_start_updata_clicked();

    void SLOT_processSendTimer(void);

private:

};

#endif // UPDATA_H
