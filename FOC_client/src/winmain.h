#ifndef WINMAIN_H
#define WINMAIN_H

#include <QMainWindow>
#include <QtSerialPort>
#include <updata.h>
#include <about.h>

#define PI 3.1415926

namespace Ui {
class Winmain;
}






class Winmain : public QMainWindow
{
    Q_OBJECT

public:
    explicit Winmain(QWidget *parent = nullptr);
    ~Winmain();
    void refreshLidarComPort(void);
    void openLidarComPort(void);
    void closeLidarComPort(void);
    void init(void);
    QTimer timer_refresh_window; //窗口更新定时器

    updata updata_ui;
    about about_ui;
    Ui::Winmain *ui;
    QByteArray rx_data;
private slots:
    void on_pb_lidar_refresh_com_list_clicked();
    void on_pb_open_lidar_serial_clicked();
    void SLOT_processUSARTData(void);

    void on_pb_read_data_clicked();

    void on_pb_write_data_clicked();

    void on_pb_reset_clicked();

    void on_pb_calibration_clicked();

    void on_pb_set_current_clicked();

    void on_pb_set_current_zero_clicked();



};

#endif // WINMAIN_H
