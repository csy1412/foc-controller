/****************************************************************************
** Meta object code from reading C++ file 'winmain.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/winmain.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'winmain.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Winmain_t {
    QByteArrayData data[11];
    char stringdata0[252];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Winmain_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Winmain_t qt_meta_stringdata_Winmain = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Winmain"
QT_MOC_LITERAL(1, 8, 36), // "on_pb_lidar_refresh_com_list_..."
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 31), // "on_pb_open_lidar_serial_clicked"
QT_MOC_LITERAL(4, 78, 21), // "SLOT_processUSARTData"
QT_MOC_LITERAL(5, 100, 23), // "on_pb_read_data_clicked"
QT_MOC_LITERAL(6, 124, 24), // "on_pb_write_data_clicked"
QT_MOC_LITERAL(7, 149, 19), // "on_pb_reset_clicked"
QT_MOC_LITERAL(8, 169, 25), // "on_pb_calibration_clicked"
QT_MOC_LITERAL(9, 195, 25), // "on_pb_set_current_clicked"
QT_MOC_LITERAL(10, 221, 30) // "on_pb_set_current_zero_clicked"

    },
    "Winmain\0on_pb_lidar_refresh_com_list_clicked\0"
    "\0on_pb_open_lidar_serial_clicked\0"
    "SLOT_processUSARTData\0on_pb_read_data_clicked\0"
    "on_pb_write_data_clicked\0on_pb_reset_clicked\0"
    "on_pb_calibration_clicked\0"
    "on_pb_set_current_clicked\0"
    "on_pb_set_current_zero_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Winmain[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    0,   65,    2, 0x08 /* Private */,
       9,    0,   66,    2, 0x08 /* Private */,
      10,    0,   67,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Winmain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Winmain *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pb_lidar_refresh_com_list_clicked(); break;
        case 1: _t->on_pb_open_lidar_serial_clicked(); break;
        case 2: _t->SLOT_processUSARTData(); break;
        case 3: _t->on_pb_read_data_clicked(); break;
        case 4: _t->on_pb_write_data_clicked(); break;
        case 5: _t->on_pb_reset_clicked(); break;
        case 6: _t->on_pb_calibration_clicked(); break;
        case 7: _t->on_pb_set_current_clicked(); break;
        case 8: _t->on_pb_set_current_zero_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Winmain::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_Winmain.data,
    qt_meta_data_Winmain,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Winmain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Winmain::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Winmain.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Winmain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
