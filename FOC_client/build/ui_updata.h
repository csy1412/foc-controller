/********************************************************************************
** Form generated from reading UI file 'updata.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATA_H
#define UI_UPDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_updata
{
public:
    QGridLayout *gridLayout_6;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout;
    QLabel *lb_version;
    QLabel *label;
    QPushButton *pb_read_version;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_3;
    QPushButton *pb_scan;
    QLabel *label_2;
    QLineEdit *le_addr;
    QGridLayout *gridLayout_5;
    QProgressBar *progressBar;
    QPushButton *pb_start_updata;

    void setupUi(QDialog *updata)
    {
        if (updata->objectName().isEmpty())
            updata->setObjectName(QString::fromUtf8("updata"));
        updata->resize(354, 124);
        gridLayout_6 = new QGridLayout(updata);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lb_version = new QLabel(updata);
        lb_version->setObjectName(QString::fromUtf8("lb_version"));

        gridLayout->addWidget(lb_version, 0, 1, 1, 1);

        label = new QLabel(updata);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        pb_read_version = new QPushButton(updata);
        pb_read_version->setObjectName(QString::fromUtf8("pb_read_version"));

        gridLayout->addWidget(pb_read_version, 0, 3, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);


        gridLayout_4->addLayout(gridLayout, 0, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        pb_scan = new QPushButton(updata);
        pb_scan->setObjectName(QString::fromUtf8("pb_scan"));

        gridLayout_3->addWidget(pb_scan, 0, 2, 1, 1);

        label_2 = new QLabel(updata);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        le_addr = new QLineEdit(updata);
        le_addr->setObjectName(QString::fromUtf8("le_addr"));

        gridLayout_3->addWidget(le_addr, 0, 1, 1, 1);

        gridLayout_3->setColumnStretch(1, 1);

        gridLayout_4->addLayout(gridLayout_3, 1, 0, 1, 1);


        gridLayout_6->addLayout(gridLayout_4, 0, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        progressBar = new QProgressBar(updata);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout_5->addWidget(progressBar, 0, 0, 1, 1);

        pb_start_updata = new QPushButton(updata);
        pb_start_updata->setObjectName(QString::fromUtf8("pb_start_updata"));

        gridLayout_5->addWidget(pb_start_updata, 0, 1, 1, 1);


        gridLayout_6->addLayout(gridLayout_5, 1, 0, 1, 1);


        retranslateUi(updata);

        QMetaObject::connectSlotsByName(updata);
    } // setupUi

    void retranslateUi(QDialog *updata)
    {
        updata->setWindowTitle(QApplication::translate("updata", "\345\233\272\344\273\266\345\215\207\347\272\247", nullptr));
        lb_version->setText(QApplication::translate("updata", "\346\234\252\347\237\245", nullptr));
        label->setText(QApplication::translate("updata", "\345\275\223\345\211\215\345\233\272\344\273\266\347\211\210\346\234\254\357\274\232", nullptr));
        pb_read_version->setText(QApplication::translate("updata", "\350\257\273\345\217\226", nullptr));
        pb_scan->setText(QApplication::translate("updata", "\346\265\217\350\247\210", nullptr));
        label_2->setText(QApplication::translate("updata", "\351\200\211\346\213\251\345\233\272\344\273\266\346\226\207\344\273\266", nullptr));
        pb_start_updata->setText(QApplication::translate("updata", "\345\274\200\345\247\213", nullptr));
    } // retranslateUi

};

namespace Ui {
    class updata: public Ui_updata {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATA_H
