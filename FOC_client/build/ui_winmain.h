/********************************************************************************
** Form generated from reading UI file 'winmain.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINMAIN_H
#define UI_WINMAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Winmain
{
public:
    QAction *ac_update;
    QAction *action_about;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QPushButton *pb_calibration;
    QPushButton *pb_write_data;
    QPushButton *pb_read_data;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLineEdit *le_id;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_21;
    QSpacerItem *horizontalSpacer_7;
    QLineEdit *le_mesure_pol;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_16;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *le_votage;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_22;
    QSpacerItem *horizontalSpacer_8;
    QLineEdit *le_r;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_17;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *le_pol;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_23;
    QSpacerItem *horizontalSpacer_9;
    QLineEdit *le_h;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_18;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *le_kp;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_24;
    QSpacerItem *horizontalSpacer_10;
    QLineEdit *le_time_out;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_19;
    QSpacerItem *horizontalSpacer_5;
    QLineEdit *le_ki;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_20;
    QSpacerItem *horizontalSpacer_6;
    QLineEdit *le_limit_current;
    QPushButton *pb_reset;
    QWidget *widget;
    QGridLayout *gridLayout;
    QLabel *lb_force_serial_state_3;
    QLabel *lb_lidar_serial_state;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_4;
    QLabel *label_25;
    QDoubleSpinBox *sb_set_current;
    QPushButton *pb_set_current;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_3;
    QLabel *label_3;
    QComboBox *cb_lidar_com_port_list;
    QPushButton *pb_lidar_refresh_com_list;
    QPushButton *pb_open_lidar_serial;
    QTextEdit *te_com;
    QPushButton *pb_set_current_zero;
    QMenuBar *menuBar;
    QMenu *me_update;
    QMenu *menu_help;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Winmain)
    {
        if (Winmain->objectName().isEmpty())
            Winmain->setObjectName(QString::fromUtf8("Winmain"));
        Winmain->setMinimumSize(QSize(633, 200));
        Winmain->setMaximumSize(QSize(16777215, 16777215));
        ac_update = new QAction(Winmain);
        ac_update->setObjectName(QString::fromUtf8("ac_update"));
        action_about = new QAction(Winmain);
        action_about->setObjectName(QString::fromUtf8("action_about"));
        centralWidget = new QWidget(Winmain);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(330, 10, 391, 241));
        pb_calibration = new QPushButton(groupBox);
        pb_calibration->setObjectName(QString::fromUtf8("pb_calibration"));
        pb_calibration->setGeometry(QRect(210, 200, 51, 31));
        pb_write_data = new QPushButton(groupBox);
        pb_write_data->setObjectName(QString::fromUtf8("pb_write_data"));
        pb_write_data->setGeometry(QRect(270, 200, 51, 31));
        pb_read_data = new QPushButton(groupBox);
        pb_read_data->setObjectName(QString::fromUtf8("pb_read_data"));
        pb_read_data->setGeometry(QRect(330, 200, 51, 31));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 30, 371, 161));
        gridLayout_2 = new QGridLayout(layoutWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        le_id = new QLineEdit(layoutWidget);
        le_id->setObjectName(QString::fromUtf8("le_id"));
        le_id->setMaximumSize(QSize(70, 16777215));

        horizontalLayout->addWidget(le_id);

        horizontalLayout->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_21 = new QLabel(layoutWidget);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        horizontalLayout_7->addWidget(label_21);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        le_mesure_pol = new QLineEdit(layoutWidget);
        le_mesure_pol->setObjectName(QString::fromUtf8("le_mesure_pol"));
        le_mesure_pol->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_7->addWidget(le_mesure_pol);

        horizontalLayout_7->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_7, 0, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_2->addWidget(label_16);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        le_votage = new QLineEdit(layoutWidget);
        le_votage->setObjectName(QString::fromUtf8("le_votage"));
        le_votage->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_2->addWidget(le_votage);

        horizontalLayout_2->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_22 = new QLabel(layoutWidget);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        horizontalLayout_8->addWidget(label_22);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_8);

        le_r = new QLineEdit(layoutWidget);
        le_r->setObjectName(QString::fromUtf8("le_r"));
        le_r->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_8->addWidget(le_r);

        horizontalLayout_8->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_8, 1, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_17 = new QLabel(layoutWidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_3->addWidget(label_17);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        le_pol = new QLineEdit(layoutWidget);
        le_pol->setObjectName(QString::fromUtf8("le_pol"));
        le_pol->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_3->addWidget(le_pol);

        horizontalLayout_3->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_23 = new QLabel(layoutWidget);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        horizontalLayout_9->addWidget(label_23);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_9);

        le_h = new QLineEdit(layoutWidget);
        le_h->setObjectName(QString::fromUtf8("le_h"));
        le_h->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_9->addWidget(le_h);

        horizontalLayout_9->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_9, 2, 1, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_18 = new QLabel(layoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_4->addWidget(label_18);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        le_kp = new QLineEdit(layoutWidget);
        le_kp->setObjectName(QString::fromUtf8("le_kp"));
        le_kp->setMaximumSize(QSize(82, 16777215));

        horizontalLayout_4->addWidget(le_kp);

        horizontalLayout_4->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_4, 3, 0, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_24 = new QLabel(layoutWidget);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        horizontalLayout_10->addWidget(label_24);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_10);

        le_time_out = new QLineEdit(layoutWidget);
        le_time_out->setObjectName(QString::fromUtf8("le_time_out"));
        le_time_out->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_10->addWidget(le_time_out);

        horizontalLayout_10->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_10, 3, 1, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_19 = new QLabel(layoutWidget);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_5->addWidget(label_19);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        le_ki = new QLineEdit(layoutWidget);
        le_ki->setObjectName(QString::fromUtf8("le_ki"));
        le_ki->setMaximumSize(QSize(82, 16777215));

        horizontalLayout_5->addWidget(le_ki);

        horizontalLayout_5->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_5, 4, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_20 = new QLabel(layoutWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_6->addWidget(label_20);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);

        le_limit_current = new QLineEdit(layoutWidget);
        le_limit_current->setObjectName(QString::fromUtf8("le_limit_current"));
        le_limit_current->setMaximumSize(QSize(70, 16777215));

        horizontalLayout_6->addWidget(le_limit_current);

        horizontalLayout_6->setStretch(1, 1);

        gridLayout_2->addLayout(horizontalLayout_6, 4, 1, 1, 1);

        pb_reset = new QPushButton(groupBox);
        pb_reset->setObjectName(QString::fromUtf8("pb_reset"));
        pb_reset->setGeometry(QRect(150, 200, 51, 31));
        widget = new QWidget(groupBox);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 200, 131, 31));
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lb_force_serial_state_3 = new QLabel(widget);
        lb_force_serial_state_3->setObjectName(QString::fromUtf8("lb_force_serial_state_3"));

        gridLayout->addWidget(lb_force_serial_state_3, 0, 0, 1, 1);

        lb_lidar_serial_state = new QLabel(widget);
        lb_lidar_serial_state->setObjectName(QString::fromUtf8("lb_lidar_serial_state"));

        gridLayout->addWidget(lb_lidar_serial_state, 0, 1, 1, 1);

        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(440, 260, 146, 31));
        gridLayout_4 = new QGridLayout(layoutWidget1);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        label_25 = new QLabel(layoutWidget1);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayout_4->addWidget(label_25, 0, 0, 1, 1);

        sb_set_current = new QDoubleSpinBox(layoutWidget1);
        sb_set_current->setObjectName(QString::fromUtf8("sb_set_current"));
        sb_set_current->setMinimum(-30.000000000000000);
        sb_set_current->setMaximum(30.000000000000000);
        sb_set_current->setSingleStep(0.100000000000000);

        gridLayout_4->addWidget(sb_set_current, 0, 1, 1, 1);

        pb_set_current = new QPushButton(centralWidget);
        pb_set_current->setObjectName(QString::fromUtf8("pb_set_current"));
        pb_set_current->setGeometry(QRect(590, 260, 51, 31));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 10, 311, 281));
        gridLayout_5 = new QGridLayout(groupBox_2);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_3->addWidget(label_3, 0, 0, 1, 1);

        cb_lidar_com_port_list = new QComboBox(groupBox_2);
        cb_lidar_com_port_list->setObjectName(QString::fromUtf8("cb_lidar_com_port_list"));
        cb_lidar_com_port_list->setMinimumSize(QSize(70, 25));

        gridLayout_3->addWidget(cb_lidar_com_port_list, 0, 1, 1, 1);

        pb_lidar_refresh_com_list = new QPushButton(groupBox_2);
        pb_lidar_refresh_com_list->setObjectName(QString::fromUtf8("pb_lidar_refresh_com_list"));
        pb_lidar_refresh_com_list->setMinimumSize(QSize(0, 25));

        gridLayout_3->addWidget(pb_lidar_refresh_com_list, 0, 2, 1, 1);

        pb_open_lidar_serial = new QPushButton(groupBox_2);
        pb_open_lidar_serial->setObjectName(QString::fromUtf8("pb_open_lidar_serial"));
        pb_open_lidar_serial->setMinimumSize(QSize(0, 25));

        gridLayout_3->addWidget(pb_open_lidar_serial, 0, 3, 1, 1);


        gridLayout_5->addLayout(gridLayout_3, 0, 0, 1, 1);

        te_com = new QTextEdit(groupBox_2);
        te_com->setObjectName(QString::fromUtf8("te_com"));

        gridLayout_5->addWidget(te_com, 1, 0, 1, 1);

        pb_set_current_zero = new QPushButton(centralWidget);
        pb_set_current_zero->setObjectName(QString::fromUtf8("pb_set_current_zero"));
        pb_set_current_zero->setGeometry(QRect(650, 260, 51, 31));
        Winmain->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Winmain);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 721, 22));
        me_update = new QMenu(menuBar);
        me_update->setObjectName(QString::fromUtf8("me_update"));
        menu_help = new QMenu(menuBar);
        menu_help->setObjectName(QString::fromUtf8("menu_help"));
        Winmain->setMenuBar(menuBar);
        statusBar = new QStatusBar(Winmain);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Winmain->setStatusBar(statusBar);

        menuBar->addAction(me_update->menuAction());
        menuBar->addAction(menu_help->menuAction());
        me_update->addAction(ac_update);
        menu_help->addAction(action_about);

        retranslateUi(Winmain);

        QMetaObject::connectSlotsByName(Winmain);
    } // setupUi

    void retranslateUi(QMainWindow *Winmain)
    {
        Winmain->setWindowTitle(QApplication::translate("Winmain", "SWUST FOC", nullptr));
        ac_update->setText(QApplication::translate("Winmain", "\344\270\213\344\275\215\346\234\272\345\215\207\347\272\247", nullptr));
        action_about->setText(QApplication::translate("Winmain", "\345\205\263\344\272\216", nullptr));
        groupBox->setTitle(QApplication::translate("Winmain", "\345\217\202\346\225\260", nullptr));
        pb_calibration->setText(QApplication::translate("Winmain", "\346\240\241\345\207\206", nullptr));
        pb_write_data->setText(QApplication::translate("Winmain", "\345\206\231\345\205\245", nullptr));
        pb_read_data->setText(QApplication::translate("Winmain", "\350\257\273\345\217\226", nullptr));
        label->setText(QApplication::translate("Winmain", "CAN ID\357\274\232", nullptr));
        label_21->setText(QApplication::translate("Winmain", "\346\265\213\351\207\217\346\236\201\345\257\271\346\225\260\357\274\232", nullptr));
        label_16->setText(QApplication::translate("Winmain", "\347\224\265\345\216\213(V):", nullptr));
        label_22->setText(QApplication::translate("Winmain", "\347\233\270\347\224\265\351\230\273(\316\251):", nullptr));
        label_17->setText(QApplication::translate("Winmain", "\346\236\201\345\257\271\346\225\260\357\274\232", nullptr));
        label_23->setText(QApplication::translate("Winmain", "\347\233\270\347\224\265\346\204\237(H):", nullptr));
        label_18->setText(QApplication::translate("Winmain", "\347\224\265\346\265\201\347\216\257P\357\274\232", nullptr));
        label_24->setText(QApplication::translate("Winmain", "CAN\351\200\232\350\256\257\350\266\205\346\227\266(ms)\357\274\232", nullptr));
        label_19->setText(QApplication::translate("Winmain", "\347\224\265\346\265\201\347\216\257I\357\274\232", nullptr));
        label_20->setText(QApplication::translate("Winmain", "\351\231\220\345\210\266\346\234\200\345\244\247\347\224\265\346\265\201(A):", nullptr));
        pb_reset->setText(QApplication::translate("Winmain", "\345\244\215\344\275\215", nullptr));
        lb_force_serial_state_3->setText(QApplication::translate("Winmain", "\344\270\262\345\217\243\347\212\266\346\200\201", nullptr));
        lb_lidar_serial_state->setText(QApplication::translate("Winmain", "\345\205\263\351\227\255", nullptr));
        label_25->setText(QApplication::translate("Winmain", "\347\224\265\346\265\201A\357\274\232", nullptr));
        pb_set_current->setText(QApplication::translate("Winmain", "\345\217\221\351\200\201", nullptr));
        groupBox_2->setTitle(QApplication::translate("Winmain", "\351\200\232\350\256\257\351\205\215\347\275\256", nullptr));
        label_3->setText(QApplication::translate("Winmain", "\344\270\262\345\217\243\345\217\267\357\274\232", nullptr));
        pb_lidar_refresh_com_list->setText(QApplication::translate("Winmain", "\345\210\267\346\226\260", nullptr));
        pb_open_lidar_serial->setText(QApplication::translate("Winmain", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        pb_set_current_zero->setText(QApplication::translate("Winmain", "\345\201\234\346\255\242", nullptr));
        me_update->setTitle(QApplication::translate("Winmain", "\345\233\272\344\273\266\345\215\207\347\272\247", nullptr));
        menu_help->setTitle(QApplication::translate("Winmain", "\345\270\256\345\212\251", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Winmain: public Ui_Winmain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINMAIN_H
