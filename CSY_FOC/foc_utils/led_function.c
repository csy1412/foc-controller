#include "led_function.h"
#include "stdint.h"
#include "interface.h"
#include "stdbool.h"
#include "param.h"


led_state_e led_state;
extern user_config_t user_config;
float led_time = 0;
int led_step = 0;


const   uint8_t led_init_light[] = {1,0};
const   float led_init_time[]={0.15,0.15};     

const   uint8_t led_show_id_light[] = {1,0,0};
const   float led_show_id_time[]={0.1,0.27,1};     

const   uint8_t led_show_null_id_light[] = {1,0};
const   float led_show_null_id_time[]={1,0.1};     

const   uint8_t led_cali_light[] = {1,0};
const   float led_cali_time[]={0.2,0.05};     

const   uint8_t led_error_light[] = {1,0};
const   float led_error_time[]={0.05,0.05};   

/**
  * @brief    led状态切换
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
 */
void ledStateSwitch(led_state_e in)
{
		if(led_state == in)
			return ;
		interfaceSetLED(0);

		led_state = in;
		led_time = 0;
		led_step = 0;
}


/**
  * @brief  	LED功能循环
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
*/

void ledFuctionLoop(void)
{
	if(led_state==led_init) //初始化音乐
	{
		led_time+=DT;
		interfaceSetLED(led_init_light[led_step]);
		if(led_time>=led_init_time[led_step])
		{
			led_time=0;
			led_step++;
		}
		if(led_step>=sizeof(led_init_light))
		{
			led_step=0;
			led_time=0;
		}
	}
	
		if(led_state==led_error) //初始化音乐
	{
		led_time+=DT;
		interfaceSetLED(led_error_light[led_step]);
		if(led_time>=led_error_time[led_step])
		{
			led_time=0;
			led_step++;
		}
		if(led_step>=sizeof(led_error_light))
		{
			led_step=0;
			led_time=0;
		}
	}
	
		if(led_state==led_cali) //初始化音乐
	{
		led_time+=DT;
		interfaceSetLED(led_cali_light[led_step]);
		if(led_time>=led_cali_time[led_step])
		{
			led_time=0;
			led_step++;
		}
		if(led_step>=sizeof(led_cali_light))
		{
			led_step=0;
			led_time=0;
		}
	}
	
	else if(led_state==led_show_id) //led
	{
		
		static int id_count=0;
		led_time+=DT;
		if(user_config.can_id>0)
		{
			interfaceSetLED(led_show_id_light[led_step]);
			if(id_count<user_config.can_id)
			{
				if(led_time>=led_show_id_time[led_step])//时间到
				{
					led_time=0;
					led_step++;
				}
				if(led_step>=sizeof(led_show_id_light)-1)  
				{
					id_count++;
					if(id_count<user_config.can_id)
					{
						led_step=0; //回去
					}
				}
			}
			else
			{
					led_step=sizeof(led_show_id_light)-1;
					if(led_time>=led_show_id_time[led_step])
					{
							id_count=0;
							led_step=0;
							led_time=0;
					}
			}		
		}
		else //ID未知 
		{
				interfaceSetLED(led_show_null_id_light[led_step]);
				if(led_time>=led_show_null_id_time[led_step])
				{
					led_time=0;
					led_step++;
				}
				if(led_step>=sizeof(led_show_null_id_light))
				{
					led_step=0;
					led_time=0;
				}
		}
	}

}
