#include "foc.h"
#include "utils.h"
#include "interface.h"
#include "param.h"
#include "led.h"

foc_data_t Foc;
extern user_config_t user_config;

/**
  * @brief   	park变换
  * @author   odrive
  * @param[in] 
  * @retval       
  */
static inline void park_transform(float Ialpha, float Ibeta, float Theta, float *Id, float *Iq)
{
	// Id =  Ialpha * cos(Theta) + Ibeta * sin(Theta)
	// Iq = -Ialpha * sin(Theta) + Ibeta * cos(Theta)
	
	float s, c;
	fast_sincos(Theta, &s, &c);
	*Id =   Ialpha * c + Ibeta * s;
  *Iq =  	-Ialpha * s + Ibeta * c;
}

/**
  * @brief   	clark变换
  * @author   odrive
  * @param[in] 
  * @retval       
  */
static inline void clarke_transform(float Ia, float Ib, float Ic, float *Ialpha, float *Ibeta)
{
	*Ialpha = Ia;
	*Ibeta  = (Ib - Ic) * ONE_BY_SQRT3;
}

/**
  * @brief   	逆Park变换
  * @author   odrive
  * @param[in] 
  * @retval       
  */
static inline void inverse_park(float mod_d, float mod_q, float Theta, float *mod_alpha, float *mod_beta)
{
		float s, c;
		fast_sincos(Theta, &s, &c);
    *mod_alpha = mod_d * c - mod_q * s;
    *mod_beta  = mod_d * s + mod_q * c;
}
/**
  * @brief   	SVM计算
  * @author   odrive
  * @param[in] 
  * @retval       
  */
static inline int svm(float alpha, float beta, float* tA, float* tB, float* tC)
{
    int Sextant;

    if (beta >= 0.0f) {
        if (alpha >= 0.0f) {
            //quadrant I
            if (ONE_BY_SQRT3 * beta > alpha)
                Sextant = 2; //sextant v2-v3
            else
                Sextant = 1; //sextant v1-v2

        } else {
            //quadrant II
            if (-ONE_BY_SQRT3 * beta > alpha)
                Sextant = 3; //sextant v3-v4
            else
                Sextant = 2; //sextant v2-v3
        }
    } else {
        if (alpha >= 0.0f) {
            //quadrant IV
            if (-ONE_BY_SQRT3 * beta > alpha)
                Sextant = 5; //sextant v5-v6
            else
                Sextant = 6; //sextant v6-v1
        } else {
            //quadrant III
            if (ONE_BY_SQRT3 * beta > alpha)
                Sextant = 4; //sextant v4-v5
            else
                Sextant = 5; //sextant v5-v6
        }
    }

    switch (Sextant) {
        // sextant v1-v2
        case 1: {
            // Vector on-times
            float t1 = alpha - ONE_BY_SQRT3 * beta;
            float t2 = TWO_BY_SQRT3 * beta;

            // PWM timings
            *tA = (1.0f - t1 - t2) * 0.5f;
            *tB = *tA + t1;
            *tC = *tB + t2;
        } break;

        // sextant v2-v3
        case 2: {
            // Vector on-times
            float t2 = alpha + ONE_BY_SQRT3 * beta;
            float t3 = -alpha + ONE_BY_SQRT3 * beta;

            // PWM timings
            *tB = (1.0f - t2 - t3) * 0.5f;
            *tA = *tB + t3;
            *tC = *tA + t2;
        } break;

        // sextant v3-v4
        case 3: {
            // Vector on-times
            float t3 = TWO_BY_SQRT3 * beta;
            float t4 = -alpha - ONE_BY_SQRT3 * beta;

            // PWM timings
            *tB = (1.0f - t3 - t4) * 0.5f;
            *tC = *tB + t3;
            *tA = *tC + t4;
        } break;

        // sextant v4-v5
        case 4: {
            // Vector on-times
            float t4 = -alpha + ONE_BY_SQRT3 * beta;
            float t5 = -TWO_BY_SQRT3 * beta;

            // PWM timings
            *tC = (1.0f - t4 - t5) * 0.5f;
            *tB = *tC + t5;
            *tA = *tB + t4;
        } break;

        // sextant v5-v6
        case 5: {
            // Vector on-times
            float t5 = -alpha - ONE_BY_SQRT3 * beta;
            float t6 = alpha - ONE_BY_SQRT3 * beta;

            // PWM timings
            *tC = (1.0f - t5 - t6) * 0.5f;
            *tA = *tC + t5;
            *tB = *tA + t6;
        } break;

        // sextant v6-v1
        case 6: {
            // Vector on-times
            float t6 = -TWO_BY_SQRT3 * beta;
            float t1 = alpha + ONE_BY_SQRT3 * beta;

            // PWM timings
            *tA = (1.0f - t6 - t1) * 0.5f;
            *tC = *tA + t1;
            *tB = *tC + t6;
        } break;
    }

    // if any of the results becomes NaN, result_valid will evaluate to false
    int result_valid =
            *tA >= 0.0f && *tA <= 1.0f
         && *tB >= 0.0f && *tB <= 1.0f
         && *tC >= 0.0f && *tC <= 1.0f;
	
    return result_valid ? 0 : -1;
}

/**
  * @brief   	把期望d和q作用在线圈上
  * @author   SiYu Chen
  * @param[in]  pwm_phase电角度
  * @retval       
  */
void apply_voltage_timings(float vbus, float v_d, float v_q, float pwm_phase)
{
	// Modulation
	float V_to_mod = 1.0f / ((2.0f / 3.0f) * vbus);
	float mod_d = V_to_mod * v_d;
	float mod_q = V_to_mod * v_q;
	
	// Vector modulation saturation, lock integrator if saturated
		float mod_scalefactor = 0.80f * SQRT3_BY_2 * 1.0f / sqrtf(mod_d * mod_d + mod_q * mod_q);
	
	
		if (mod_scalefactor < 1.0f) {
				mod_d *= mod_scalefactor;
				mod_q *= mod_scalefactor;
		}
	
	// Inverse park transform
	float mod_alpha;
	float mod_beta;
		
	inverse_park(mod_d, mod_q, pwm_phase, &mod_alpha, &mod_beta);
	svm(mod_alpha, mod_beta, &Foc.dtc_a, &Foc.dtc_b, &Foc.dtc_c);
	interfaceSetPWM(Foc.dtc_a,Foc.dtc_b,Foc.dtc_c);
	
}


/**
  * @brief   	FOC电流控制
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */

void FOC_current(foc_data_t *foc, float Id_des, float Iq_des, 
															float I_phase, float pwm_phase)
{
	
	// Clarke transform
	float i_alpha, i_beta;
	clarke_transform(-foc->i_a,-foc->i_b,-foc->i_c, &i_alpha, &i_beta);
	// Park transform
	float i_d, i_q;
	park_transform(i_alpha, i_beta, I_phase, &i_d, &i_q);
	
	// Current Filter used for report
	foc->i_d_filt = 0.95f*foc->i_d_filt + 0.05f*i_d;
	foc->i_q_filt = 0.95f*foc->i_q_filt + 0.05f*i_q;
	
	// Apply PI control
	float Ierr_d = Id_des - i_d;
	float Ierr_q = Iq_des - i_q;

	float v_d = user_config.current_ctrl_p_gain * Ierr_d + foc->current_ctrl_integral_d;
	float v_q = user_config.current_ctrl_p_gain * Ierr_q + foc->current_ctrl_integral_q;

	// Modulation
	float mod_to_V = (2.0f / 3.0f) * foc->v_bus;
	float V_to_mod = 1.0f / mod_to_V;
	float mod_d = V_to_mod * v_d;
	float mod_q = V_to_mod * v_q;

	// Vector modulation saturation, lock integrator if saturated
   float  mod_scalefactor = 0.8f * SQRT3_BY_2 * 1.0f / sqrtf(mod_d * mod_d + mod_q * mod_q);
    if (mod_scalefactor < 1.0f) {
        mod_d *= mod_scalefactor;
        mod_q *= mod_scalefactor;
        foc->current_ctrl_integral_d *= 0.99f;
        foc->current_ctrl_integral_q *= 0.99f;
    } else {
        foc->current_ctrl_integral_d += Ierr_d * (user_config.current_ctrl_i_gain );
        foc->current_ctrl_integral_q += Ierr_q * (user_config.current_ctrl_i_gain );
    }
	
  foc->i_bus_filt = mod_d * foc->i_d_filt + mod_q * foc->i_q_filt;

	float mod_alpha;
  float mod_beta;
	inverse_park(mod_d, mod_q, pwm_phase, &mod_alpha, &mod_beta);


	float dtc_a, dtc_b, dtc_c;
	svm(mod_alpha, mod_beta, &dtc_a, &dtc_b, &dtc_c);

  interfaceSetPWM(dtc_a,dtc_b,dtc_c);
//	DEBUG("dq:%f,%f\r\n",i_d,i_q);

//	LED1=!LED1;
}



/**
  * @brief   	FOC变量复位
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void focPidReset(foc_data_t *foc)
{
		foc->i_d_filt = 0;
		foc->i_q_filt = 0;
		foc->current_ctrl_integral_d = 0;
		foc->current_ctrl_integral_q = 0;

}


