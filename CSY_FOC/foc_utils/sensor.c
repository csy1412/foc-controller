#include "sensor.h"
#include "interface.h"
#include "param.h"
#include "utils.h"
encoder_t encoder;
extern user_config_t user_config;


/**
  * @brief    编码器校准
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
 */
void encoderSample(void)
{
	
	int32_t delta_enc = 0;
	
	//encoder.raw = ENCODER_CPR-interfaceGetEncoder(); //获取原始数据
	encoder.raw_dis=encoder.raw ;

	if(user_config.calib_valid) //如果已经校准通过
	{
			//偏移
			int off_1 = user_config.offset_lut[encoder.raw>>encoder.bit_offset]; //查表找到当前值
			int off_2 = user_config.offset_lut[((encoder.raw>>encoder.bit_offset)+1)%128]; //查表找到下一个值

			int off_interp = off_1 + ((off_2 - off_1) * (encoder.raw - ((encoder.raw>>encoder.bit_offset)<<encoder.bit_offset)) >> encoder.bit_offset);		// Interpolate between lookup table entries
			int cnt = encoder.raw - off_interp;	// Correct for nonlinearity with lookup table from calibration
			if(cnt > ENCODER_CPR)
			{
				cnt -= ENCODER_CPR;
			}
			else if(cnt < 0)
			{
				cnt += ENCODER_CPR;
			}
				encoder.cnt = cnt; //转成正数
	}
	else
	{
		encoder.cnt = encoder.raw;
	}

	delta_enc = encoder.cnt - encoder.count_in_cpr_;
	delta_enc = mod(delta_enc, ENCODER_CPR);
	
	if (delta_enc > ENCODER_CPR_DIV_2)
	{
		delta_enc -= ENCODER_CPR;  //减去一圈
	}

	encoder.shadow_count_ += delta_enc;
	encoder.count_in_cpr_ += delta_enc;
	encoder.count_in_cpr_ = mod(encoder.count_in_cpr_, ENCODER_CPR);

	encoder.count_in_cpr_ = encoder.cnt;

	//// run pll (for now pll is in units of encoder counts)
	// Predict current pos
	encoder.pos_estimate_counts_ += DT * encoder.vel_estimate_counts_;//预测当前值
	encoder.pos_cpr_counts_      += DT * encoder.vel_estimate_counts_;
	// discrete phase detector

 float	delta_pos_counts = (float)(encoder.shadow_count_ - (int32_t)floor(encoder.pos_estimate_counts_));
 float delta_pos_cpr_counts = (float)(encoder.count_in_cpr_ - (int32_t)floor(encoder.pos_cpr_counts_));

	delta_pos_cpr_counts = wrap_pm(delta_pos_cpr_counts, ENCODER_CPR_DIV_2);
	// pll feedback
	encoder.pos_estimate_counts_ += DT * encoder.pll_kp_ * delta_pos_counts;
	encoder.pos_cpr_counts_ += DT * encoder.pll_kp_ * delta_pos_cpr_counts;
	encoder.pos_cpr_counts_ = fmodf_pos(encoder.pos_cpr_counts_, (float)(ENCODER_CPR));
	encoder.vel_estimate_counts_ += DT * encoder.pll_ki_ * delta_pos_cpr_counts;
	bool snap_to_zero_vel = false;
	if (fabs(encoder.vel_estimate_counts_) < 0.5f * DT * encoder.pll_ki_) {
			encoder.vel_estimate_counts_ = 0.0f;  // align delta-sigma on zero to prevent jitter
			snap_to_zero_vel = true;
	}

	// Outputs from encoder for Controller
	float pos_cpr_last = encoder.pos_cpr_;
	encoder.pos_estimate_ = encoder.pos_estimate_counts_ / (float)ENCODER_CPR;
	encoder.vel_estimate_ = encoder.vel_estimate_counts_ / (float)ENCODER_CPR;
	encoder.pos_cpr_= encoder.pos_cpr_counts_ / (float)ENCODER_CPR;
	float delta_pos_cpr = wrap_pm(encoder.pos_cpr_ - pos_cpr_last, 0.5f);

	//// run encoder count interpolation
	int32_t corrected_enc = encoder.count_in_cpr_ - user_config.encoder_offset;
	// if we are stopped, make sure we don't randomly drift
	if (snap_to_zero_vel) {
			encoder.interpolation_ = 0.5f;
	// reset interpolation if encoder edge comes
	// TODO: This isn't correct. At high velocities the first phase in this count may very well not be at the edge.
	} else if (delta_enc > 0) {
			encoder.interpolation_ = 0.0f;
	} else if (delta_enc < 0) {
			encoder.interpolation_ = 1.0f;
	} else {
			// Interpolate (predict) between encoder counts using vel_estimate,
			encoder.interpolation_ += DT * encoder.vel_estimate_counts_;
			// don't allow interpolation indicated position outside of [enc, enc+1)
			if (encoder.interpolation_ > 1.0f) encoder.interpolation_ = 1.0f;
			if (encoder.interpolation_ < 0.0f) encoder.interpolation_ = 0.0f;
	}
	
	float interpolated_enc = corrected_enc + encoder.interpolation_;

	//// compute electrical phase
	float elec_rad_per_enc = user_config.motor_pole_pairs * M_2PI * (1.0f / (float)ENCODER_CPR);
	float ph = elec_rad_per_enc * interpolated_enc;
	encoder.phase_ = wrap_pmm_pi(ph);

}


/**
  * @brief    编码器参数初始化
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void encoderParamInit(void)
{
	interfaceDelayMs(10);
	encoder.bit_offset = ENCODER_BIT-7;
	// Init
	encoder.count_in_cpr_ = 0;
	encoder.shadow_count_ = 0;
	encoder.pos_estimate_counts_ = 0.0f;
	encoder.vel_estimate_counts_ = 0.0f;
	encoder.pos_cpr_counts_ = 0.0f;

	encoder.pos_estimate_ = 0.0f;
	encoder.vel_estimate_ = 0.0f;
	encoder.pos_cpr_ = 0.0f;

	encoder.phase_ = 0.0f;
	encoder.interpolation_ = 0.0f;

	int encoder_pll_bandwidth = 200;
	encoder.pll_kp_ = 2.0f * encoder_pll_bandwidth;  				// basic conversion to discrete time
	encoder.pll_ki_ = 0.25f * (encoder.pll_kp_ * encoder.pll_kp_); 	// Critically damped
	
}


/**
  * @brief    获取电流偏移
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void getCurrentZero(foc_data_t *foc)
{
		int adc_sum_a = 0;
		int adc_sum_b = 0;
		int adc_sum_c = 0;
		int n = 1000;
		for (int i = 0; i<n; i++){	// Average n samples of the ADC
			interfaceDelayUs(60);
			adc_sum_a += foc->adc_phase_a;
			adc_sum_b += foc->adc_phase_b;
			adc_sum_c += foc->adc_phase_c;
		}
		foc->adc_phase_a_offset = adc_sum_a/n;
		foc->adc_phase_b_offset = adc_sum_b/n;
		foc->adc_phase_c_offset = adc_sum_c/n;
}


