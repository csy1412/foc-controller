#ifndef __MUSIC_H
#define __MUSIC_H

typedef enum
{
		music_ready=0,
		init_music,
		error_music,
		set_id_music
}music_state_e;

void musicStateSwitch(music_state_e in);
void playMusicLoop(void);



#endif

