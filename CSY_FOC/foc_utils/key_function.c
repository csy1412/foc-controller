#include "key_function.h"
#include "stdint.h"
#include "interface.h"

double check_time=0; //检测功能
key_state_e key_state=set_calibration; //按键状态 
extern motor_state_e motor_state; //电机状态
extern user_config_t user_config;

/**
  * @brief   按键检测功能循环
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
*/

void keyFuctionLoop(void)
{
		check_time+=DT; //检测时间
	
		static int key_press = 0;
		static int key_press_num = 0;
		static double press_last_time = 0; //按下的时间
	  static double loosen_last_time = 0; //松开的时间
		static double press_time = 0; 
		static int last_key_value = 0;
	
		int key_value = interfaceKeyScan(); //扫描按键时间
		
		if(key_value==1&&last_key_value==0) //按键按下
		{
			press_last_time = check_time; //记录当前时间
		}	
		else if(last_key_value==1&&key_value==0) //按键按下
		{
			loosen_last_time = check_time; //记录当前时间
		}	
		
		if(key_value==1) //按键按下
			 press_time+=DT;
		else
			 press_time=0; //清零
		
		if(last_key_value==1&&key_value==0
			&&loosen_last_time-press_last_time>0.1&&loosen_last_time-press_last_time<2
			&&key_state==set_calibration)
		{
					key_press_num=0;
   				key_state=set_id;  //进入ID设置
  				ledStateSwitch(led_null);
				  musicStateSwitch(set_id_music);  //进入准备模式
          DEBUG("Enter ID set mode\r\n");	
		}
		
		else if(key_state==set_id)
		{
				interfaceSetLED(key_value);
		 		if(last_key_value==1&&key_value==0
				&&loosen_last_time-press_last_time>0.05)
				{
						key_press_num++; //id增加		
				}	
				else if(key_value==0&&check_time-loosen_last_time>4) //4秒不按下
				{			
					
						 key_state=set_calibration;		
						 DEBUG("Exit ID set Mode\r\n"); 
						 DEBUG("Motor ID = %d \r\n",key_press_num);
						 user_config.can_id=key_press_num;
						 saveConfig();
						 key_press_num=0;
						 motorStateSwitch(runing);
						 interfaceResetMCU();
				}
		}
		
		if(motor_state!=calibration&&key_state==set_calibration&&press_time>=5) //如果是
		{
					press_time=0;
					motorStateSwitch(calibration);  //进入校准模式	
		}	
	
		last_key_value=key_value;
}
