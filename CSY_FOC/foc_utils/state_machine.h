#ifndef __STATE_MACHINE_H
#define __STATE_MACHINE_H

#include "stdint.h"
#include "stdbool.h"

// Errors
#define ERR_OVER_VOLTAGE	((int)1<<0) //过压
#define	ERR_UNDER_VOLTAGE	((int)1<<1) //低压
#define	ERR_OVER_SPEED		((int)1<<2) //过速
#define	ERR_FLASH					((int)1<<3) //FLASH读取出错
#define	ERR_STALL					((int)1<<4) //电机堵转
#define	ERR_LOST_ENCODER	((int)1<<5) //编码器丢失


typedef enum
{
	null=-1,
	ready=0,
	error,
	runing,
	update,
	calibration,
	beep
}motor_state_e;

typedef struct {
	float input_current;
	int motor_error_code;
	bool input_source; //1是来自CAN 0是串口
} controller_t;

void MotorTaskLoop(void);
void motorStateSwitch(motor_state_e in);
void motorStateReset(void);

#endif




