#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H

#include "stdint.h"

#pragma pack(push)
#pragma pack(1)

/* 
head = 0xFA
cmd = 0x00 复位
cmd = 0x01 读取flash 写入flash data[0]=0 写入   data[0]=1读取
		
*/

typedef struct
{
	  uint8_t head;
    uint8_t size;
    uint8_t cmd;
		uint8_t data[100];
	
}usart_data_pack_t;

typedef struct
{
	uint16_t id;
	float voltage;
	uint16_t pole;
	float kp;
	float ki;
	uint16_t measure_pole;
	float R;
	float H;
	uint16_t can_time_out;
	float max_current;
}param_data_pack_t;


#pragma pack(pop)


/* CAN send and receive ID */
typedef enum
{
    CAN_MOTO_1_TO_4_ID = 0x200, //1-4电机ID
		CAN_MOTO_5_TO_8_ID = 0x1FF,
		CAN_MOTO_9_TO_12_ID = 0x1FE,
		CAN_MOTO_13_TO_16_ID = 0x1FD,
		CAN_MOTO_CALI_ID = 0x3F0
} can_msg_id_e;



void usartReciveHandle(uint8_t *data,int size);
void canSendDataLoop(void);
void canReciveData(uint32_t StdId,uint8_t Data[8],uint8_t DLC);
void canGetCurrent(uint32_t StdId,uint8_t Data[8],uint8_t DLC);
#endif





