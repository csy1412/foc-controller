#include "param.h"
#include "utils.h"
#include "interface.h"
#include "state_machine.h"
user_config_t user_config;
extern controller_t controller;




/**
  * @brief   	装载默认参数
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void loadDefaultConfig(void)
{
	// Motor
	user_config.pole_pairs_auto_check=1;//默认自动打开
	user_config.motor_pole_pairs = 7;
	user_config.motor_phase_resistance = 0.05f;
	user_config.motor_phase_inductance = 0.000015f;
	user_config.inertia = 0.001f;
	
	// Calib
	user_config.calib_valid = 0;
	user_config.calib_current = 1.0f;
	user_config.calib_max_voltage = 3.0f;
	user_config.scan_num=6; //扫描的电角度
	
	// Anticogging
	user_config.anticogging_enable = 1;
	user_config.anticogging_pos_threshold = 0.001f;
	user_config.anticogging_vel_threshold = 0.1f;

	// Control
	user_config.control_mode = 0;
	user_config.current_ramp_rate = 2.0f;
	user_config.vel_ramp_rate = 50.0f;
	user_config.traj_vel = 30;
	user_config.traj_accel = 100;
	user_config.traj_decel = 100;
	user_config.pos_gain = 120.0f;
	user_config.vel_gain = 2.0f;
	user_config.vel_integrator_gain = 0.2f;
	user_config.vel_limit = 90;
	user_config.current_limit = 2.5;
	user_config.current_ctrl_p_gain = 0;
	user_config.current_ctrl_i_gain = 0;
	user_config.current_ctrl_bandwidth = 100;  //为电流环1/20
	user_config.voltage=12.0f;
	
	// Protect
	user_config.protect_under_voltage = 12;
	user_config.protect_over_voltage  = 40;
	user_config.protect_over_speed = 300;
	
	// Can
	user_config.can_id = 0;
	user_config.can_timeout_ms = 500;
	user_config.can_reply_frequence = 1000;
	user_config.can_sync_target_enable = 0;
}

/**
  * @brief   	保存配置文件
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void saveConfig(void)
{
		user_config.crc = crc32((uint8_t*)&user_config, sizeof(user_config)-4, 0);
	  interfaceWriteFlash((uint8_t*)&user_config,sizeof(user_config));
		DEBUG("Flash parm size = %d byte \r\n",sizeof(user_config));
		DEBUG("Flash writing!!\r\n");
}

/**
  * @brief    读取配置文件
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void readConfig(void)
{
	DEBUG("Flash parm size = %d byte \r\n",sizeof(user_config));
	DEBUG("Flash reading!!\r\n");
	interfaceReadFlash((uint8_t*)&user_config,sizeof(user_config));
	uint32_t crc = crc32((uint8_t*)&user_config, sizeof(user_config)-4, 0);
	if(crc != user_config.crc){
		user_config.calib_valid=0;
		controller.motor_error_code|=ERR_FLASH; //校准出错
		loadDefaultConfig(); 
		DEBUG("Flash read error!! \r\nload default param!!\r\n");
	}
	else
	{
		DEBUG("Flash read successful !!\r\n");
		controller.motor_error_code&=~ERR_FLASH; 
	}

}
