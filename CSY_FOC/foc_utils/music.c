#include "music.h"
#include "interface.h"
#include "sensor.h"
#include "utils.h"
#include "calibration.h"
extern encoder_t encoder;
extern user_config_t user_config;
extern foc_data_t Foc;


const   u16 tone[] = {247,262,294,330,349,392,440,494,523,587,659,698,784,1000,0};

const   u8 music_set_id[]={8,14};            
const   float  time_set_id[] = {0.2,1.2};

const   u8 music_init[]={7,3,5,6,7,9};            
const   float  time_init[] = {0.2,0.1,0.1,0.1,0.1,0.3};

/****************校准音乐*****************/
const   u8 music_cali[]={3,5,3,5};            
const   float time_cali[] = {0.1,0.1,0.1,0,1};

music_state_e music_state;
int step=0;
double time=0;

/**
  * @brief   播放音乐循环
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
 */
void playMusicLoop(void)
{

	if(music_state==init_music) //初始化音乐
	{
				time+=DT;
				apply_voltage_timings(Foc.v_bus,0,0.1,encoder.phase_);
				interfaceSetFreq(tone[music_init[step]]);
				if(time>=time_init[step])
				{
					time=0;
					step++;
				}
				if(step>=sizeof(music_init)) //完成
				{
					step=0;
					time=0;
					musicStateSwitch(music_ready);
					interfaceSetRuningParam();//电机设置成为运行模式
					if(!user_config.calib_valid)
					{
						//loadDefaultConfig(); //开启上电未校准自动校准
					 	//motorStateSwitch(calibration);//进入准备模式
						motorStateSwitch(error);//进入错误模式
						
					}
					else
					{
						DEBUG("Calibration is not necessary !!!\r\n");
						motorStateSwitch(runing);//进入准备模式
					}
				}		
	}

	else if(music_state==set_id_music)
	{
				time+=DT;
				if(tone[music_set_id[step]]==0)
					apply_voltage_timings(Foc.v_bus,0,0.0,encoder.phase_);
				else
					apply_voltage_timings(Foc.v_bus,0,0.1,encoder.phase_);
				
				interfaceSetFreq(tone[music_set_id[step]]);
				
				if(time>=time_set_id[step])
				{
					time=0;
					step++;
				}
				if(step>=sizeof(music_set_id)) //完成
				{
					step=0;
					time=0;
				}		

	}

}


/**
  * @brief    音乐状态切换
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
 */
void musicStateSwitch(music_state_e in)
{
		
		if(in==init_music)
		{
			music_state=init_music;
			interfaceMotorArm();
			motorStateSwitch(beep); //进入蜂鸣器模式
		}
		else if(in==error_music)
		{
			music_state=error_music;
			interfaceMotorArm();
			motorStateSwitch(beep); //进入蜂鸣器模式
		}
		else if(in==set_id_music)
		{
			music_state=set_id_music;
			interfaceMotorArm();
			motorStateSwitch(beep); //进入蜂鸣器模式
		}

		
}


