#ifndef __PARAM_H
#define __PARAM_H

#include "interface.h"

#pragma pack(push)
#pragma pack(1)

typedef struct {
	// Motor
	int pole_pairs_auto_check;	// (Auto)
	int motor_pole_pairs;			// (Auto)
	float motor_phase_resistance;	// (Auto)
	float motor_phase_inductance;	// (Auto)
	float inertia;					// [A/(turn/s^2)]

	// Encoder
	int encoder_dir;				// (Auto)
	int encoder_offset;				// (Auto)
	int scan_num;				// 扫描多少个电角度
	int offset_lut[OFFSET_LUT_NUM];	// (Auto)

	// Calib
	int calib_valid;				// (Auto)
	float calib_current;			// [A]
	float calib_max_voltage;		// [V]

	// Anticogging
	int anticogging_enable;
	float anticogging_pos_threshold;
	float anticogging_vel_threshold;

	// Control
	int control_mode;
	float current_ramp_rate;		// [A/sec]
	float vel_ramp_rate;			// [(turn/s)/s]
	float traj_vel;					// [turn/s]
	float traj_accel;				// [(turn/s)/s]
	float traj_decel;				// [(turn/s)/s]
	float pos_gain;					// [(turn/s)/turn]
	float vel_gain;					// [A/(turn/s)]
	float vel_integrator_gain;		// [A/((turn/s)*s)]
	float vel_limit;				// [turn/s]
	float current_limit;			// [A]
	float current_ctrl_p_gain;		// (Auto)
	float current_ctrl_i_gain;		// (Auto)
	int current_ctrl_bandwidth; 	// [rad/s] Current loop bandwidth 100~2000
	float voltage; 
	// Protect
	float protect_under_voltage;	// [V]
	float protect_over_voltage;		// [V]
	float protect_over_speed;		// [turn/s]

	// CAN
	int can_id;						// CAN bus ID
	int can_timeout_ms;				// CAN bus timeout in ms : 0 Disable
	int can_sync_target_enable;		// 0 Disable : 1 Enable
	uint32_t can_reply_frequence;		//can通讯频率
	
	uint32_t crc;
} user_config_t;
#pragma pack(pop)

void saveConfig(void);
void readConfig(void);
void loadDefaultConfig(void);
#endif






