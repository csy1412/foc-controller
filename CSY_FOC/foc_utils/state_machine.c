#include "state_machine.h"
#include "calibration.h"
#include "param.h"
#include "utils.h"
#include "sensor.h"
#include "music.h"


motor_state_e motor_state=ready; //电机状态
motor_state_e motor_last_state=ready; //电机状态
controller_t controller={0};


extern foc_data_t Foc;
extern user_config_t user_config;
extern encoder_t encoder;

void MotorTaskLoop(void)
{
		Foc.system_time+= DT; //系统时间
	
		if(motor_state==calibration) //如果电机是在校准模式
		{
			CalibrationLoop(&Foc); //校准循环
		}
		else if(motor_state==beep) //蜂鸣
		{
			playMusicLoop();  //蜂鸣器鸣叫
		}
		else if(motor_state==runing) //运行模式
		{

				float current_setpoint = CLAMP(controller.input_current, -user_config.current_limit, user_config.current_limit);
				float phase_vel = M_2PI * encoder.vel_estimate_ * user_config.motor_pole_pairs;	
				phase_vel = phase_vel * user_config.encoder_dir;
				current_setpoint = current_setpoint * user_config.encoder_dir;
				float phase = encoder.phase_ * user_config.encoder_dir;
				float pwm_phase = phase + 1.5f * DT * phase_vel; //速度补偿
				FOC_current(&Foc, 0,current_setpoint , phase, pwm_phase);

  			//过速保护
				if(fabs(encoder.vel_estimate_) > user_config.protect_over_speed)
				{
					controller.motor_error_code |= ERR_OVER_SPEED;
				}
			
		}
		else if(motor_state==error) //错误
		{
				ledStateSwitch(led_error);
				interfaceMotorDisArm();
		}
}

/**
  * @brief   	电机状态切换
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void motorStateSwitch(motor_state_e in)
{
	if(in==motor_state)
	{
		return ;
	}
	if(in==runing)
	{
		if(user_config.calib_valid==0)
			{
				DEBUG("Motor not calibrated,Please confirm calibration !!!\r\n");
				return;
			}
			motorStateReset();
			interfaceMotorArm();
			interfaceSetRuningParam();
			ledStateSwitch(led_show_id); 
			DEBUG("Motor start running !!!\r\n");
	 }
	 else if (in==ready)
	 {
				interfaceMotorDisArm();
				DEBUG("Motor has ready to run !!!\r\n");
	 } 
	 	 else if (in==beep)
	 {
				DEBUG("Motor enter beep mode !!!\r\n");
	 }
	 else if(in == calibration)
	 {
				ledStateSwitch(led_cali); 
				calibrationStart(); //校准开始
	 }
		motor_last_state=motor_state;
		motor_state=in;
		motorStateReset();
}


/**
  * @brief   	复位电机
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void motorStateReset(void)
{
	controller.input_current=0;
	Foc.current_ctrl_integral_d=0;
	Foc.current_ctrl_integral_q=0;

}

