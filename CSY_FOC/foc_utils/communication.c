#include "communication.h"
#include "interface.h"
#include "calibration.h"
#include "safe_check.h"
#include "string.h"

extern user_config_t user_config;
extern foc_data_t Foc;
extern controller_t controller;
extern motor_state_e motor_state; //电机状态
extern encoder_t encoder;



/**
  * @brief    串口接受中断
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
inline void usartReciveHandle(uint8_t *data,int size)
{
	usart_data_pack_t usart_data_pack;
	if(data[0]!=FRAM_HEAD) //返回
		return;
	switch(data[2])
	{
  		case 0x00: //复位
				if(size!=3)
					return;
				
				break;
			case 0x01: //读取 写入 FLASH
				if(data[3]==1&&data[1]==4) //读取FLASH
				{
					usart_data_pack_t usart_data_pack_temp;
					usart_data_pack_temp.head=FRAM_HEAD;
					usart_data_pack_temp.size=(3+sizeof(param_data_pack_t));
					usart_data_pack_temp.cmd = 0x01;
					param_data_pack_t param_data_pack_temp;
					param_data_pack_temp.id=user_config.can_id;
					param_data_pack_temp.can_time_out=user_config.can_timeout_ms;
					param_data_pack_temp.H=user_config.motor_phase_inductance;
					param_data_pack_temp.kp=user_config.current_ctrl_p_gain;
					param_data_pack_temp.ki=user_config.current_ctrl_i_gain;
					param_data_pack_temp.max_current=user_config.current_limit;
					param_data_pack_temp.measure_pole=user_config.scan_num;
					param_data_pack_temp.pole=user_config.motor_pole_pairs;
					param_data_pack_temp.R=user_config.motor_phase_resistance;
					param_data_pack_temp.voltage=user_config.voltage;
					memcpy(usart_data_pack_temp.data,&param_data_pack_temp,sizeof(param_data_pack_t));
					
					interfaceUsartSend((uint8_t *)&usart_data_pack_temp,usart_data_pack_temp.size);
				}
		
					
				break;	
			case 0x02: //校准
				if(size!=3)
					return;
//				if(motor_state!=calibration)
//	
				break;
			case 0x03: 
				if(size!=data[1])
					return;
				memcpy(&controller.input_current,&data[3],4);
				controller.input_source=0; //设置为串口输入
				break;
			case 0x04: 
				if(data[1]==3+sizeof(param_data_pack_t)) //写入
				{
					param_data_pack_t param_data_pack_temp;
					memcpy(&param_data_pack_temp,&data[3],sizeof(param_data_pack_t));
					user_config.can_id=param_data_pack_temp.id;
					user_config.can_timeout_ms=param_data_pack_temp.can_time_out;
					user_config.motor_phase_inductance=param_data_pack_temp.H;
					user_config.current_ctrl_p_gain=param_data_pack_temp.kp;
					user_config.current_ctrl_i_gain=param_data_pack_temp.ki;
					user_config.current_limit=param_data_pack_temp.max_current;
					user_config.scan_num=param_data_pack_temp.measure_pole;
					user_config.motor_pole_pairs=param_data_pack_temp.pole;
					user_config.motor_phase_resistance=param_data_pack_temp.R;
					user_config.voltage=param_data_pack_temp.voltage;
					saveConfig();
						
				 }
					break;

	}

}



/**
  * @brief    CAN发送
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */

void canSendDataLoop(void)
{
		static float  last_time = 0;
		if(Foc.system_time-last_time<(1/(float)(user_config.can_reply_frequence)))
		{
				return ; 
		}
		last_time =  Foc.system_time;
			
			if(user_config.can_id>0||motor_state==runing)
			{	
				uint32_t self_id = 0x200 + user_config.can_id;
				uint8_t buff[8]={0};
				uint16_t angle =encoder.pos_cpr_*ENCODER_CPR;
				
				buff[0]=angle>>8;
				buff[1]=angle;
				int16_t speed=(encoder.vel_estimate_*60);
				buff[2]=speed>>8;
				buff[3]=speed;
				int16_t current=(Foc.i_q_filt*1000);
				buff[4]=current>>8;
				buff[5]=current;
				buff[6]=37;
				buff[7]=0;
				interfaceCanTrasnsport(self_id,buff,8);
		}
}

/**
  * @brief    CAN接受
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void canReciveData(uint32_t StdId,uint8_t Data[8],uint8_t DLC)
{
		switch(StdId)
			 {
					 case CAN_MOTO_1_TO_4_ID:
								canGetCurrent(StdId,Data,DLC); 
						break;
					 case CAN_MOTO_5_TO_8_ID:
								canGetCurrent(StdId,Data,DLC); 
						break;
					 case CAN_MOTO_9_TO_12_ID:
								canGetCurrent(StdId,Data,DLC); 
						break;
					 case CAN_MOTO_13_TO_16_ID:
								canGetCurrent(StdId,Data,DLC); 
						break;
					 case CAN_MOTO_CALI_ID:  //CAN通讯校准功能
								if(motor_state!=calibration&&Data[0]=='c'&&user_config.can_id==Data[1])
								{
										motorStateSwitch(calibration);//进入校准模式
								}
						break;
					 
				default:break;
			}
}


/**
  * @brief   通过can获取电流
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void canGetCurrent(uint32_t StdId,uint8_t Data[8],uint8_t DLC)
{
	if(user_config.can_id<=0||user_config.can_id>16||DLC>8) //不正常就退出
			return;
	if(StdId==CAN_MOTO_1_TO_4_ID&&user_config.can_id<=4) //前4个电机
	{
		int id=user_config.can_id-0;
		controller.input_current=
						((int16_t)(Data[2*id-2]<<8|Data[2*id-1]))/1000.0f;
		canCheckTimeHook();
	}
	else if(StdId==CAN_MOTO_5_TO_8_ID&&user_config.can_id>=5&&user_config.can_id<=8) 
	{
		int id=user_config.can_id-4;
		controller.input_current=
						((int16_t)(Data[2*id-2]<<8|Data[2*id-1]))/1000.0f;
		canCheckTimeHook();
	}	
	else if(StdId==CAN_MOTO_9_TO_12_ID&&user_config.can_id>=9&&user_config.can_id<=12) 
	{
		int id=user_config.can_id-8;
		controller.input_current=
						((int16_t)(Data[2*id-2]<<8|Data[2*id-1]))/1000.0f;
		canCheckTimeHook();
	}	
	else if(StdId==CAN_MOTO_13_TO_16_ID&&user_config.can_id>=13&&user_config.can_id<=16) 
	{
		int id=user_config.can_id-12;
		controller.input_current=
						((int16_t)(Data[2*id-2]<<8|Data[2*id-1]))/1000.0f;
		canCheckTimeHook();
	}
	  controller.input_source=1; //来自CAN
}


