#ifndef __SENSOR_H
#define __SENSOR_H

#include "stdint.h"

#include "stdbool.h"
#include "foc.h"
typedef struct {
	uint16_t raw;
	uint16_t offset;
	float raw_dis;
	uint8_t bit_offset;
	int cnt;
	int pos_abs_;
	int count_in_cpr_;
	int shadow_count_;
	float pos_estimate_counts_;
	float vel_estimate_counts_;
	float pos_cpr_counts_;
	
	float pos_estimate_;
  float vel_estimate_;
	float pos_cpr_;
	
	float phase_;
	float interpolation_;

	float pll_kp_;
	float pll_ki_;
	
} encoder_t;

void encoderSample(void);
void getCurrentZero(foc_data_t *foc);
void encoderParamInit(void);

#endif

