#include "calibration.h"
#include "interface.h"
#include "param.h"
#include "sensor.h"
#include "stdlib.h"
#include "heap.h"
#include "state_machine.h"
#include "utils.h"


calibrate_step_e calibrate_step = CS_NULL; // 校准步骤
calibrate_error_e calibrate_error = CE_NULL; // 校准错误

extern motor_state_e motor_state; //电机状态
extern user_config_t user_config;
extern encoder_t encoder;
int *p_error_arr = NULL;
extern foc_data_t Foc;


/**
  * @brief    开始校准
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
*/

void calibrationStart(void)
{
	if(p_error_arr == NULL) //如果误差数组
	{
		p_error_arr = HEAP_malloc(SAMPLES_PER_PPAIR*MOTOR_POLE_PAIRS_MAX*4);
	}
	user_config.calib_valid = false;
	calibrate_step = CS_MOTOR_R_START;

}



/**
  * @brief    校准结束
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
 */
void CalibrationStop(void)
{
	calibrate_step = CS_NULL;
	interfaceMotorDisArm();
	motorStateSwitch(error); //进入校准模式
	HEAP_free(p_error_arr);
	p_error_arr = NULL;
}


/**
  * @brief    校准循环
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
 */

void CalibrationLoop(foc_data_t *foc)
{
	
	if(motor_state!=calibration)
		return;
	
	static uint32_t loop_count;

	// R
	static const float kI = 2.0f;
	static const int num_R_cycles = CURRENT_MEASURE_HZ * 2;

	// L
	static float voltages[2];
	static float Ialphas[2];
	static const uint32_t num_L_cycles = CURRENT_MEASURE_HZ / 2;

	static int raw_offset;
	static int sample_count;
	static float next_sample_time;
	static float theta_ref;
	static float start_count;
	static float end_count;

	float time = (float)(loop_count*DT);
	

	
	switch(calibrate_step)
		{
		case CS_NULL:
			break;
		
		case CS_MOTOR_R_START: 
			loop_count = 0;
			voltages[0] = 0.0f;
			interfaceMotorArm();
			calibrate_step = CS_MOTOR_R_LOOP;
			DEBUG("/**********calibration start!!!***********/ \r\n");
			break;
		
		case CS_MOTOR_R_LOOP: //开始测量线圈内阻
			voltages[0] += (kI * DT) * (user_config.calib_current - (-foc->i_a)); //期望校准电流
			if (voltages[0] > user_config.calib_max_voltage || voltages[0] < -user_config.calib_max_voltage)
			{
				interfaceMotorDisArm();
				DEBUG("over set votage!!\r\n");	
				calibrate_error = CE_PHASE_RESISTANCE_OUT_OF_RANGE;
				calibrate_step = CS_ERROR;
				break;
			}		
			// 使用电压在相A上
			apply_voltage_timings(foc->v_bus,voltages[0],0, 0.0f);			
			// 达到2s进入下一步
			if(loop_count >= num_R_cycles) 
			{
				interfaceMotorDisArm();
				calibrate_step = CS_MOTOR_R_END;
			}
			break;
			
		case CS_MOTOR_R_END:
			user_config.motor_phase_resistance = (voltages[0] / user_config.calib_current) * (2.0f / 3.0f);
			DEBUG("phase R = %f\r\n", user_config.motor_phase_resistance);
			calibrate_step = CS_MOTOR_L_START; //进入电感测量过程
			break;
		case CS_MOTOR_L_START:
			Ialphas[0] = 0.0f;
			Ialphas[1] = 0.0f;
			voltages[0] = +user_config.calib_max_voltage;  
			voltages[1] = -user_config.calib_max_voltage;  
			loop_count = 0;
			interfaceMotorArm();
			apply_voltage_timings(foc->v_bus,voltages[0],0, 0);
			calibrate_step = CS_MOTOR_L_LOOP; //进入电感循环
			break;
		
		case CS_MOTOR_L_LOOP:
			{
				int i = loop_count & 1;
				Ialphas[i] += (-foc->i_a);
				
				// Test voltage along phase A
				apply_voltage_timings(foc->v_bus, voltages[i],0, 0.0f);
				
				if(loop_count >= (num_L_cycles<<1)){
					interfaceMotorDisArm();
					calibrate_step = CS_MOTOR_L_END;
				}
			}
			break;
			
		case CS_MOTOR_L_END:  //电感总结
			{
				float dI_by_dt = (Ialphas[1] - Ialphas[0]) / (float)(DT * num_L_cycles);
				float L = user_config.calib_max_voltage / dI_by_dt;
				user_config.motor_phase_inductance = L * (2.0f / 3.0f);
				DEBUG("phase L = %f\r\n", user_config.motor_phase_inductance);
				updateCurrentGain();
				calibrate_step = CS_ENCODER_DIR_PP_START;
			}
			break;
			
		case CS_ENCODER_DIR_PP_START:  //进入方向检测状态
			voltages[0] = user_config.calib_current * user_config.motor_phase_resistance * 3.0f / 2.0f;;
			interfaceMotorArm();
			loop_count = 0;
			time=0;
			calibrate_step = CS_ENCODER_DIR_PP_LOCK;
			break;
			
		case CS_ENCODER_DIR_PP_LOCK: //记录开始编码器位置
			apply_voltage_timings(foc->v_bus, (voltages[0] * time / 2.0f),0, 0);
			if (time >= 2){
				start_count = encoder.shadow_count_; 
				loop_count = 0;
				time=0;
				calibrate_step = CS_ENCODER_DIR_PP_LOOP;
				break;
			}
			break;
		
		case CS_ENCODER_DIR_PP_LOOP: 
			apply_voltage_timings(foc->v_bus,voltages[0],0, (CALB_SPEED*time));
			if (time >= (2.0*user_config.scan_num)*M_PI/CALB_SPEED)
				{
					end_count = encoder.shadow_count_;
					apply_voltage_timings(foc->v_bus, voltages[0], 0, 0);
					calibrate_step = CS_ENCODER_DIR_PP_END;
					break;
			}
			break;
			
		case CS_ENCODER_DIR_PP_END: //检查极对数
			{	
				int diff = end_count - start_count;
				if(user_config.pole_pairs_auto_check) //如果打开自动检测
				{
					 user_config.motor_pole_pairs = round((2.0f*user_config.scan_num)/fabsf(diff/(float)ENCODER_CPR)/2);
				}
				DEBUG("pole pairs = %d\r\n", user_config.motor_pole_pairs);
				if(user_config.motor_pole_pairs > MOTOR_POLE_PAIRS_MAX){
					calibrate_error = CE_MOTOR_POLE_PAIRS_OUT_OF_RANGE;
					calibrate_step = CS_ERROR;
					break;
			 }
				// 检查方向
				if(diff > +10){
					user_config.encoder_dir = 1;
					DEBUG("dir = CW\r\n");
				}else if(diff < -10){
					user_config.encoder_dir = -1;
					DEBUG("dir = CCW\r\n");
				}
				else
				{
					DEBUG("Encoder data has no change!!\r\n");
				}
					
			}
			calibrate_step = CS_ENCODER_OFFSET_START; //检测编码器偏移
			break;
			
		case CS_ENCODER_OFFSET_START:  //检测编码器偏移
			loop_count = 0;
			time=0;
			calibrate_step = CS_ENCODER_OFFSET_LOCK;
			break;
		
		case CS_ENCODER_OFFSET_LOCK:
			apply_voltage_timings(foc->v_bus,(voltages[0] * time / 2.0f),0,  0);
			if (time >= 2){
				theta_ref = 0;
				loop_count = 0;
				time=0;
				sample_count = 0;
				next_sample_time = 0;
				raw_offset = encoder.raw;
				calibrate_step = CS_ENCODER_OFFSET_CW_LOOP;
				break;
				}
			 break;
		case CS_ENCODER_OFFSET_CW_LOOP: //检查偏移循环
			theta_ref += CALB_SPEED * DT;  //期望电角度自加 
			apply_voltage_timings(foc->v_bus,voltages[0],0,theta_ref); //转到这里
			// sample SAMPLES_PER_PPAIR times per pole-pair
			if(time >= next_sample_time) //当到达采样时间了
				{
					int count_ref = ENCODER_CPR * (theta_ref/(float)(M_2PI*user_config.motor_pole_pairs)); 
					//将电机从电机零度转到11个极对所占全部的电角度中编码器的位置
					//也就是当前电角度对应的编码器值
					int error;
					if(user_config.encoder_dir == +1)
					{
						error = encoder.raw - count_ref; //当前值减去电角度对应编码器值
					}
					else if(user_config.encoder_dir == -1)
					{
						error = (ENCODER_CPR - encoder.raw) - count_ref;
					}
				p_error_arr[sample_count] = error + ENCODER_CPR * (error<0); //存起来
				sample_count ++;
				if(sample_count >= user_config.motor_pole_pairs*SAMPLES_PER_PPAIR)
				{
					sample_count --;
				}
				next_sample_time += M_2PI/(CALB_SPEED*SAMPLES_PER_PPAIR);
			}

			if (time >= M_2PI*user_config.motor_pole_pairs/CALB_SPEED) //完成这次
			{
				next_sample_time = 0;
				loop_count = 0;
				time = 0;
				calibrate_step = CS_ENCODER_OFFSET_CCW_LOOP;
				break;
			}
			break;
			
		case CS_ENCODER_OFFSET_CCW_LOOP:  //反转
			// rotate voltage vector through one mechanical rotation in the negative direction
			theta_ref -= CALB_SPEED * DT;
			apply_voltage_timings(foc->v_bus,voltages[0],0, theta_ref);
		
			// sample SAMPLES_PER_PPAIR times per pole-pair
			if(time >= next_sample_time){
				int count_ref = ENCODER_CPR * (theta_ref/(float)(M_2PI*user_config.motor_pole_pairs));
				int error;
				if(user_config.encoder_dir == +1){
					error = encoder.raw - count_ref;
				}else if(user_config.encoder_dir == -1){
					error = (ENCODER_CPR - encoder.raw) - count_ref;
				}
				error = error + ENCODER_CPR * (error<0);
				p_error_arr[sample_count] = (p_error_arr[sample_count] + error)/2;
				sample_count --;
				if(sample_count <= 0){
					sample_count = 0;
				}
				next_sample_time += M_2PI/(CALB_SPEED*SAMPLES_PER_PPAIR);
			}

			if (time >= M_2PI*user_config.motor_pole_pairs/CALB_SPEED){
				raw_offset += encoder.raw; //起始角度
				raw_offset /= 2;
				interfaceMotorDisArm();
				calibrate_step = CS_ENCODER_OFFSET_END;
				break;
			}
			break;
			
		case CS_ENCODER_OFFSET_END:
			{
				// Calculate average offset
				int64_t ezero_mean = 0;
				for(int i = 0; i<(user_config.motor_pole_pairs*SAMPLES_PER_PPAIR); i++)
				{
					ezero_mean += p_error_arr[i];
				}
				user_config.encoder_offset = ezero_mean/(user_config.motor_pole_pairs*SAMPLES_PER_PPAIR);
				DEBUG("encoder offset = %d\r\n",  user_config.encoder_offset);
					
				// Moving average to filter out cogging ripple
				int window = SAMPLES_PER_PPAIR;
				raw_offset = OFFSET_LUT_NUM * raw_offset / ENCODER_CPR; //起始所占序数
				for(int i = 0; i<OFFSET_LUT_NUM; i++)
				{
					int moving_avg = 0;
					for(int j = (-window)/2; j<(window)/2; j++){
						int index = i*user_config.motor_pole_pairs*SAMPLES_PER_PPAIR/OFFSET_LUT_NUM + j;
						if(index < 0){
							index += (SAMPLES_PER_PPAIR*user_config.motor_pole_pairs);
						}else if(index > (SAMPLES_PER_PPAIR*user_config.motor_pole_pairs-1)){
							index -= (SAMPLES_PER_PPAIR*user_config.motor_pole_pairs);
						}
						moving_avg += p_error_arr[index];
					}
					moving_avg = moving_avg/window;
					int lut_index = raw_offset + i;
					if(lut_index > (OFFSET_LUT_NUM-1))
					{
						lut_index -= OFFSET_LUT_NUM;
					}
						user_config.offset_lut[lut_index] = moving_avg - user_config.encoder_offset;
				}	
				user_config.calib_valid = true;
				calibrate_step = CS_NULL; //复位
				finishCalibration();
			}
			break;
		
		case CS_ERROR:
			DEBUG("calibration error!!\r\n code=%d \r\n", calibrate_step);
			CalibrationStop();
			calibrate_step = CS_NULL;
			break;
		default:
			break;
	}	
	loop_count ++;
}



/**
  * @brief   	FOC更新电感 电阻增益设置
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void updateCurrentGain(void)
{
	float AB = Foc.v_bus*SHUNT_RESISTENCE*MAGNIFICATION/3.3;
	
	user_config.current_ctrl_p_gain = user_config.motor_phase_inductance*user_config.current_ctrl_bandwidth/AB;
	user_config.current_ctrl_i_gain = user_config.motor_phase_resistance * user_config.current_ctrl_bandwidth/AB*DT;
}

/**
  * @brief   	校准正常完成
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void finishCalibration(void)
{
		saveConfig();
		DEBUG("/**********calibration finished!!!***********/ \r\n");
		interfaceResetMCU();//重启
		//motorStateSwitch(runing);//进入准备模式
}

