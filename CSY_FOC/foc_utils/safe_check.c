#include "safe_check.h"
#include "interface.h"
#include "state_machine.h"


extern user_config_t user_config;
extern motor_state_e motor_state; //电机状态
extern controller_t controller;

double safe_check_time_base=0;
double last_can_connect_time;

/**
  * @brief   	安全检测循环
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void safeCheckLoop(void)
{
		safe_check_time_base+=DT;
		checkCanDisconnect();
	

}


/**
  * @brief   	检测电机在线
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void checkCanDisconnect(void)
{
		/*如果时间差超过一定时间并且是在运动模式下，置零电流期望*/
		if(safe_check_time_base-last_can_connect_time>
			(float)(user_config.can_timeout_ms/1000.0f)
			&&motor_state==runing&&controller.input_source==1) 
		{
				controller.input_current=0;
		}

}

/**
  * @brief   	在线检测的HOOK
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void canCheckTimeHook(void)
{
		last_can_connect_time=safe_check_time_base;
}
