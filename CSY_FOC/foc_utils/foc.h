#ifndef __FOC_H
#define __FOC_H

#include <math.h>
#include <stdint.h>


typedef struct
{
		uint16_t adc_vbus;   //总线采样电压
		int adc_phase_a, adc_phase_b, adc_phase_c;         //三相采样电压
		float v_bus;                                            // DC总电压
		float i_a, i_b, i_c;                                    // 相电流
		float dtc_a, dtc_b, dtc_c;
		float i_d_filt, i_q_filt, i_bus_filt;                   // D/Q currents
		float current_ctrl_integral_d, current_ctrl_integral_q;	// Current error integrals
		int adc_phase_a_offset;
		int adc_phase_b_offset;
		int adc_phase_c_offset;
		int auto_loder;                //当前自动重装载值
		float system_time;
}foc_data_t;



void apply_voltage_timings(float vbus, float v_d, float v_q, float pwm_phase);
void FOC_current(foc_data_t *foc, float Id_des, float Iq_des, 
															float I_phase, float pwm_phase);
void focPidReset(foc_data_t *foc);

#endif




