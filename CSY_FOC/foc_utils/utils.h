#ifndef __UTILS_H
#define __UTILS_H
#include "math.h"
#include "stdint.h"
#define M_PI_BY_2 1.57079632679
#define M_PI_BY_3 1.0471975512
#define M_PI					(3.14159265358f)
#define M_2PI					(6.28318530716f)
#define M_3PI_BY_2 4.71238898038
#define SQRT3 					(1.73205080757f)
#define SQRT3_BY_2				(0.86602540378f)
#define ONE_BY_SQRT3			(0.57735026919f)
#define TWO_BY_SQRT3			(1.15470053838f)
#define SQ(x) 					((x) * (x))
#define ABS(x) 					( (x)>0?(x):-(x) ) 
#define MIN(a,b) 				(((a)<(b))?(a):(b))
#define MAX(a,b) 				(((a)>(b))?(a):(b))
#define CLAMP(x, lower, upper) 	(MIN(upper, MAX(x, lower)))
#define _round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))
#define _constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
#define MaxLimit(a,max) ((a)>(max)?(max):(a))

#define MinLimit(a,min) ((a)<(min)?(min):(a))

#define LIMIT_VAL(a,min,max) ((a)<(min)?(min):((a)>(max)?(max):(a)))


#define true 1
#define false 0



int mod(int dividend, int divisor);
float wrap_pm(float x, float pm_range);
float wrap_pmm_pi(float theta);
float wrap_pm_2pi(float theta);
void fast_sincos(float angle, float *sin, float *cos);
float fmodf_pos(float x, float y);
unsigned int crc32(const unsigned char *buf, int len, unsigned int init);
float _normalizeAngle(float angle);


#endif





