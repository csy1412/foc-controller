#ifndef __CALIBRATION_H
#define __CALIBRATION_H	 


#include "interface.h"
#include "foc.h"


typedef enum {
	CS_NULL = 0,
	
	CS_MOTOR_R_START,
	CS_MOTOR_R_LOOP,
	CS_MOTOR_R_END,
	
	CS_MOTOR_L_START,
	CS_MOTOR_L_LOOP,
	CS_MOTOR_L_END,
	
	CS_ENCODER_DIR_PP_START,
	CS_ENCODER_DIR_PP_LOCK,
	CS_ENCODER_DIR_PP_LOOP,
	CS_ENCODER_DIR_PP_END,
	
	CS_ENCODER_OFFSET_START,
	CS_ENCODER_OFFSET_LOCK,
	CS_ENCODER_OFFSET_CW_LOOP,
	CS_ENCODER_OFFSET_CCW_LOOP,
	CS_ENCODER_OFFSET_END,
	
	CS_ERROR,
}calibrate_step_e;

typedef enum {
	CE_NULL = 0,
	CE_PHASE_RESISTANCE_OUT_OF_RANGE, //超相电压
	CE_MOTOR_POLE_PAIRS_OUT_OF_RANGE  //超极对数

}calibrate_error_e;

void CalibrationLoop(foc_data_t *foc);
void calibrationStart(void);
void CalibrationStop(void);
void updateCurrentGain(void);
void finishCalibration(void);
#endif

