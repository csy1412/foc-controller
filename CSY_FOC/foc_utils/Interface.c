#include "interface.h"

extern foc_data_t Foc;


/**
  * @brief   	复位接口 请在内部实现单片机复位
  * @author   SiYu Chen
  * @param[in]
  * @retval
  */
inline void interfaceResetMCU(void)
{
		__set_FAULTMASK(1);
		NVIC_SystemReset();	
}

/**
  * @brief   	CAN通讯发送 请在内部实现CAN发送
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
	
inline void interfaceCanTrasnsport(uint32_t std,u8* msg,u8 len)
{
	 Can_Send_Msg(std,msg,len);
}

/**
  * @brief   	CAN接受	请将此函数放在CA接受中断中	
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
inline void interfaceCanReciveDataHandle(uint32_t StdId,uint8_t Data[8],uint8_t DLC)
{
	canReciveData(StdId,Data,DLC);
}

/**
  * @brief   	设置LED灯	请在内部实现电平切换
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void interfaceSetLED(int state_in)
{
	if(state_in==1)
	{
			GPIO_ResetBits(GPIOC,GPIO_Pin_13);          //高电平使能
	}
	else
	{
			GPIO_SetBits(GPIOC,GPIO_Pin_13);          //高电平使能
	}
}


/**
  * @brief   	按键功能检测
	*						内部实现按键按下检测
	*						为按下返回1
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
int interfaceKeyScan(void)
{
	if(GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_15)==1)
		return 1;
	else
		return 0;		
}

/**
  * @brief   	电机主循环调用接口	
  *	          需要用户在中断里面快速调用  频率2K
	*						这个函数中完成ADC采样，编码器采样 电机任务循环
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void interfaceMotorLoop(void)
{
		encoderSample();
		interfaceGetAdc();
		MotorTaskLoop();
		canSendDataLoop(); //CAN发送
		ledFuctionLoop();
		safeCheckLoop();
		keyFuctionLoop();
}


/**
  * @brief   	电机解锁	 请在内部实现电机使能	
  * @author   SiYu Chen
  * @param[in]  pwm_phase电角度
  * @retval       
  */
void interfaceMotorArm(void)
{
		GPIO_SetBits(GPIOC,GPIO_Pin_14);          //高电平使能
		TIM_Cmd(TIM2,ENABLE);
}


/**
  * @brief   	电机上锁	 请在内部实现电机失能
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void interfaceMotorDisArm(void)
{
		GPIO_ResetBits(GPIOC,GPIO_Pin_14);        //低电平失能
		TIM_Cmd(TIM2,DISABLE);
}

/**
  * @brief   	读取电压接口	 请在内部读取两相电压
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
inline void interfaceGetAdc(void)
{
		Foc.adc_vbus = 783; //换算出v_bus等于12
		Foc.adc_phase_a =  ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_1);
		Foc.adc_phase_b =  ADC_GetInjectedConversionValue(ADC1,ADC_InjectedChannel_2);
		Foc.v_bus = 0.96f*Foc.v_bus + 0.04f*Foc.adc_vbus*V_SCALE;  //加入低通滤波
	
		float a = ((I_SCALE * (float)(Foc.adc_phase_a - Foc.adc_phase_a_offset)));
		float b = (-I_SCALE * (float)(Foc.adc_phase_b - Foc.adc_phase_b_offset));
		float c = -a-b;
		
	
		Foc.i_a= a;
		Foc.i_b= b;
		Foc.i_c= c;
	

}

/**
  * @brief   	读取编码器接口	请在内部实现编码器读取
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
uint16_t interfaceGetEncoder(void)
{
	uint16_t raw_data = I2C_getRawCount(I2C1); //获取原始数据
	return raw_data;
}


/**
  * @brief   	设置三相高电平时间  请在内部实现PWM设置
	*						在其中完成PWM设置
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void interfaceSetPWM(float t1,float t2,float t3)
{
		//在其中实现PWM设置
		TIM_SetCompare1(TIM2,(uint16_t)(t1 * (float)Foc.auto_loder));
		TIM_SetCompare2(TIM2,(uint16_t)(t2 * (float)Foc.auto_loder));
		TIM_SetCompare3(TIM2,(uint16_t)(t3 * (float)Foc.auto_loder));
}


/**
  * @brief   	设置定时器分频系数					
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void interfaceSetPrescaler(int Prescaler)
{
	TIM_PrescalerConfig(TIM2,Prescaler-1,TIM_PSCReloadMode_Update);
}



/**
  * @brief   	设置重装载值					
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */
void interfaceSetAutolodaer(int autoloader)
{
	Foc.auto_loder=autoloader;
	TIM_SetAutoreload(TIM2,autoloader);
}

/**
  * @brief   	设置三相高电平时间
	*						在其中完成PWM设置
  * @author   SiYu Chen
  * @param[in]  pwm_phase电角度
  * @retval       
  */
void interfaceSetFreq(int frq)
{
	if(frq==0)
	{
		return;
	}

	TIM_PrescalerConfig(TIM2,144-1,TIM_PSCReloadMode_Update);
	
	u16 arr = 500000/frq;
	if(500000/frq>16383)
	{
			arr=16383;
	}
	if(500000/frq<200)
	{
			arr=200;
	}

	interfaceSetAutolodaer(arr);
}


/**
  * @brief   	foc ms 延时函数		
  * @author   SiYu Chen
  * @param[in]  pwm_phase电角度
  * @retval       
  */
void interfaceDelayMs(int time)
{
		delay_ms(time);
}


/**
  * @brief   	foc us 延时函数
  * @author   SiYu Chen
  * @param[in]  pwm_phase电角度
  * @retval       
  */
void interfaceDelayUs(int time)
{
		delay_us(time);
}

/**
  * @brief    设置成电机模式	
  * @author   SiYu Chen
  * @param[in]  pwm_phase电角度
  * @retval       
  */
void interfaceSetRuningParam(void)
{
	interfaceSetPrescaler(1);
	interfaceSetAutolodaer(PWM_ARR);
	interfaceSetPWM(0.0,0.0,0.0);
}


/**
  * @brief   	读取Flash接口	
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
int interfaceReadFlash(uint8_t * data,int size)
{

	structReadFromFlash((u8 *)data,size,STM32_USERDATA_BASE);   	
	
	return 1;
}


/**
  * @brief   	写入Flash接口	
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
int interfaceWriteFlash(uint8_t * data,int size)
{
	structWriteToFlash((u8 *)data,size,STM32_USERDATA_BASE);   	
	
	return 1;
}


/**
  * @brief   	FOC初始化
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
void interfaceFocInit(void)
{
	protocol_init();
	DEBUG("\r\n \r\n \r\n \r\n");
	DEBUG("/**********      SWUST FOC INIT    ***********/\r\n\r\n\r\n\r\n");
	ledStateSwitch(led_init);
	getCurrentZero(&Foc);//获取电流0飘
	encoderParamInit();  //编码器
	readConfig();
	focPidReset(&Foc);   //pid参数初始化
	musicStateSwitch(init_music);

	DEBUG("\r\n \r\n \r\n \r\n");	
	//生成网址 http://patorjk.com/software/taag/#p=display&f=Slant&t=i%20think%20of%20you
	DEBUG("    __________  ______   ______       ____  _____________    \r\n");
	DEBUG("   / ____/ __ \\/ ____/  / ___/ |     / / / / / ___/_  __/   \r\n");
	DEBUG("  / /_  / / / / /       \\__ \\| | /| / / / / /\\__ \\ / /   \r\n");
	DEBUG(" / __/ / /_/ / /___    ___/ /| |/ |/ / /_/ /___/ // /         \r\n");
	DEBUG("/_/    \\____/\\____/   /____/ |__/|__/\\____//____//_/        \r\n");																								
 	DEBUG("\r\n \r\n \r\n \r\n");                                                        
	
	DEBUG("version %s (2022-5-13) \r\n",APPLICATION_VERSION);
	DEBUG("design by Be stronger \r\n");
	DEBUG("\r\n \r\n \r\n \r\n");
	
}

/**
  * @brief   	串口接受中断
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
void interfaceUsartHandle(uint8_t *buff,uint32_t size)
{
   protocol_data_recv(buff,size);
	 receiving_process(); //解析一次
}


/**
  * @brief   	串口发送
  * @author   SiYu Chen
  * @param[in]  
  * @retval
  */
void interfaceUsartSend(uint8_t *buff,int size)
{
		USART1_Send(buff,	size);
}
