#ifndef __INTERFACE_H
#define __INTERFACE_H

/****************接口头文件********************/
#include "stm32f10x_tim.h"
#include "stm32f10x_adc.h"
#include "delay.h"
#include "iic.h"
#include "stmflash.h"
#include "stdio.h"
#include "usart.h"
/****************接口宏定义********************/
#define PWM_ARR					2880       //20KHz
#define DT						(1.0f/2000.0f)
#define LOOP_FREQUNCE					2000 //主循环频率2K
#define DEBUG(...) if(ENABLE_DEBUG)(printf(__VA_ARGS__))

/****************配置定义**********************/
#define APPLICATION_VERSION "1.03"  //软件版本
#define ENABLE_DEBUG 1

/****************编码器定义********************/
#define ENCODER_BIT			 12       //编码器位数
#define ENCODER_CPR			 4096     //编码器一圈的脉冲
#define ENCODER_CPR_DIV_2	(ENCODER_CPR>>1)  //除以2

/****************采样定义********************/
#define SHUNT_RESISTENCE	0.01f						// 采样电阻大小
#define MAGNIFICATION	50.0f								// 放大倍数
#define V_SCALE (19.0f * 3.3f / 4095.0f)   // Bus volts per A/D Count (0.015311 V)
#define I_SCALE ((3.3f / 4095.0f) / SHUNT_RESISTENCE / MAGNIFICATION)	// Amps per A/D Count
#define CURRENT_MEASURE_HZ		2000

/****************校准定义********************/
#define OFFSET_LUT_NUM		128
#define CALB_SPEED				M_2PI		// 校准时的速度 in rad/s
#define MOTOR_POLE_PAIRS_MAX	24   //检测出的最大极对数
#define SAMPLES_PER_PPAIR		OFFSET_LUT_NUM  //总共采样


#define FRAM_HEAD 0xFA
#define FRAM_RETURN_HEAD 0xFB //串口返回帧头



#include "stdint.h"
#include "state_machine.h"
#include "foc.h"
#include "sensor.h"
#include "communication.h"
#include "music.h"
#include "param.h"
#include "key_function.h"
#include "led_function.h"
#include "safe_check.h"
#include "can.h"
#include "struct_typedef.h"
#include "protool.h"

void interfaceResetMCU(void);
void interfaceSetLED(int state_in);
int interfaceKeyScan(void);
void interfaceMotorArm(void);
void interfaceMotorDisArm(void);
uint16_t interfaceGetEncoder(void);
void interfaceSetPWM(float t1,float t2,float t3);
void interfaceDelayMs(int time);
void interfaceDelayUs(int time);
void interfaceSetFreq(int frq);
void interfaceMotorLoop(void);
void interfaceGetAdc(void);
void interfaceFocInit(void);
int interfaceReadFlash(uint8_t * data,int size);
int interfaceWriteFlash(uint8_t * data,int size);
void interfaceUsartHandle(uint8_t *buff,uint32_t size);
void interfaceSetPrescaler(int Prescaler);
void interfaceSetAutolodaer(int autoloader);
void interfaceSetRuningParam(void);
void interfaceCanReciveDataHandle(uint32_t StdId,uint8_t Data[8],uint8_t DLC);
void interfaceCanTrasnsport(uint32_t std,u8* msg,u8 len);
void interfaceUsartSend(uint8_t *buff,int size);
#endif




