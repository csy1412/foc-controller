#include "app.h"
#include "interface.h"


RCC_ClocksTypeDef get_rcc_clock;   //这行设置为全局变量

/**
  * @brief   	外设初始化
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void BspInit(void)
{
	RCC_GetClocksFreq(&get_rcc_clock); 
	delay_init();	    	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(115200);	
	LED_Init();			    
	KEY_Init();        
	GPIO_ENABLE_Init(); 
	TIM2_PWM_Init();    
	//SPI2_Init();    
	SENSOR_I2C_Init();
	TIM4_20KHZ_Init();
	CURRENT_SENSOR_Init();
	CAN_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_3tq,3,CAN_Mode_Normal);
}

/**
  * @brief   	参数初始化
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void ParamInit(void)
{

	TIM_Cmd(TIM4,ENABLE);
	interfaceFocInit();

}





//////Sine Wave PWM ///////////////////
//const int pwmSin[] = {180,183,186,189,193,196,199,202,
//		205,208,211,214,217,220,224,227,
//		230,233,236,239,242,245,247,250,
//		253,256,259,262,265,267,270,273,
//		275,278,281,283,286,288,291,293,
//		296,298,300,303,305,307,309,312,
//		314,316,318,320,322,324,326,327,
//		329,331,333,334,336,337,339,340,
//		342,343,344,346,347,348,349,350,
//		351,352,353,354,355,355,356,357,
//		357,358,358,359,359,359,360,360,
//		360,360,360,360,360,360,360,359,
//		359,359,358,358,357,357,356,355,
//		355,354,353,352,351,350,349,348,
//		347,346,344,343,342,340,339,337,
//		336,334,333,331,329,327,326,324,
//		322,320,318,316,314,312,309,307,
//		305,303,300,298,296,293,291,288,
//		286,283,281,278,275,273,270,267,
//		265,262,259,256,253,250,247,245,
//		242,239,236,233,230,227,224,220,
//		217,214,211,208,205,202,199,196,
//		193,189,186,183,180,177,174,171,
//		167,164,161,158,155,152,149,146,
//		143,140,136,133,130,127,124,121,
//		118,115,113,110,107,104,101,98,
//		95,93,90,87,85,82,79,77,
//		74,72,69,67,64,62,60,57,
//		55,53,51,48,46,44,42,40,
//		38,36,34,33,31,29,27,26,
//		24,23,21,20,18,17,16,14,
//		13,12,11,10,9,8,7,6,
//		5,5,4,3,3,2,2,1,
//		1,1,0,0,0,0,0,0,
//		0,0,0,1,1,1,2,2,
//		3,3,4,5,5,6,7,8,
//		9,10,11,12,13,14,16,17,
//		18,20,21,23,24,26,27,29,
//		31,33,34,36,38,40,42,44,
//		46,48,51,53,55,57,60,62,
//		64,67,69,72,74,77,79,82,
//		85,87,90,93,95,98,101,104,
//		107,110,113,115,118,121,124,127,
//		130,133,136,140,143,146,149,152,
//		155,158,161,164,167,171,174,177};


////int sin_divider = 2;
//int phase_A_position=0;
//int phase_B_position=120;
//int phase_C_position=240;
//int step_delay  = 100;
//char stepper_sine = 0;
//int forward = 1;
//int gate_drive_offset = 0;
//int GIMBAL_MODE=0;


//#define 	SINE_DIVIDER 				3
//void advanceincrement()
//{
//if (!forward){
//	phase_A_position ++;
//    if (phase_A_position > 359){
//	   phase_A_position = 0 ;
//    }
//	    phase_B_position ++;
//	     if (phase_B_position > 359){
//		phase_B_position = 0 ;
//	}
//	    phase_C_position ++;
//	     if (phase_C_position > 359){
//		phase_C_position = 0 ;
//	}
//}else{
//	phase_A_position --;
//	    if (phase_A_position < 0){
//		   phase_A_position = 359 ;
//	    }
//		    phase_B_position --;
//		     if (phase_B_position < 0){
//			phase_B_position = 359;
//		}
//		    phase_C_position --;
//		     if (phase_C_position < 0){
//			phase_C_position = 359 ;
//		}
//}
////    if(GIMBAL_MODE){
////    TIM2->CCR1 = ((2*pwmSin[phase_A_position])+gate_drive_offset)*PWM_Period/2000;
////    TIM2->CCR2 = ((2*pwmSin[phase_B_position])+gate_drive_offset)*PWM_Period/2000;
////    TIM2->CCR3 = ((2*pwmSin[phase_C_position])+gate_drive_offset)*PWM_Period/2000;
////    }else{
////    TIM2->CCR1 = ((2*pwmSin[phase_A_position]/SINE_DIVIDER)+gate_drive_offset)*PWM_Period/2000;
////    TIM2->CCR2 = ((2*pwmSin[phase_B_position]/SINE_DIVIDER)+gate_drive_offset)*PWM_Period/2000;
////    TIM2->CCR3 = ((2*pwmSin[phase_C_position]/SINE_DIVIDER)+gate_drive_offset)*PWM_Period/2000;
////    }
//}

//long map(long x, long in_min, long in_max, long out_min, long out_max)
//{
//	if (x < in_min){
//		x = in_min;
//	}
//	if (x > in_max){
//		x = in_max;
//	}
//	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
//}
