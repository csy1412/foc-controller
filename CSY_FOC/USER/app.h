#ifndef __APP_H
#define __APP_H

#include "delay.h"
#include "usart.h"
#include "led.h"
#include "key.h"
#include "adc.h"
#include "timer.h"
#include "gpio.h"
//#include "fsm.h"
#include "foc.h"
#include "beep.h"
#include "iic.h"
#include "spi.h"
#include "can.h"


void BspInit(void);
void ParamInit(void);


#endif


