#ifndef __CAN_H
#define __CAN_H	 
#include "sys.h"
#include "stdint.h"




u8 CAN_Mode_Init(u8 tsjw,u8 tbs2,u8 tbs1,u16 brp,u8 mode);
void USB_LP_CAN1_RX0_IRQHandler(void);
u8 Can_Send_Msg(uint32_t std,u8* msg,u8 len);


#endif
