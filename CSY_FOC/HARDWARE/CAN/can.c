#include "can.h"
#include "stm32f10x_can.h"
#include "interface.h"

/**
  * @brief   	CAN通讯初始化
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
u8 CAN_Mode_Init(u8 tsjw,u8 tbs2,u8 tbs1,u16 brp,u8 mode)
{
	GPIO_InitTypeDef			GPIO_InitStructure;
	CAN_InitTypeDef       CAN_InitStructure;
	CAN_FilterInitTypeDef  CAN_FilterInitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	
	GPIO_Init(GPIOB,&GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	
	GPIO_PinRemapConfig(GPIO_Remap1_CAN1,ENABLE);
	
	CAN_DeInit(CAN1);
	CAN_StructInit(&CAN_InitStructure);
	
	CAN_InitStructure.CAN_ABOM = DISABLE;				//是否使用自动离线管理
	CAN_InitStructure.CAN_AWUM = DISABLE;				//是否使用睡眠自动唤醒
	CAN_InitStructure.CAN_BS1 = tbs1;							//影响波特率
	CAN_InitStructure.CAN_BS2 = tbs2;							//影响波特率
	CAN_InitStructure.CAN_Mode = mode;					//正常模式
	CAN_InitStructure.CAN_NART = DISABLE;				//是否使用非自动重传输
	CAN_InitStructure.CAN_Prescaler = brp;				//影响波特率					48M/((7+8+1)*6)=500K
	CAN_InitStructure.CAN_RFLM = DISABLE;				//是否使用FIFO锁定（覆盖旧数据）
	CAN_InitStructure.CAN_SJW = tsjw;		//重新同步跳跃宽度（影响波特率）
	CAN_InitStructure.CAN_TTCM = DISABLE;				//是否使用时间触发模式
	CAN_InitStructure.CAN_TXFP = ENABLE;				//是否使用发送优先级（由ID决定）
	
	CAN_Init(CAN1,&CAN_InitStructure);
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

	CAN_FilterInitStructure.CAN_FilterNumber=0;	  //过滤器0
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit; //32位
	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO0;//过滤器0关联到FIFO0
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE; //激活过滤器0

	CAN_FilterInit(&CAN_FilterInitStructure);//滤波器初始化

	NVIC_InitTypeDef NVIC_InitStructure;
	/*中断设置*/
	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;	   //CAN1 RX0中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;		   //抢占优先级0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;			   //子优先级为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);

	return 0;
}


/**
  * @brief   	CAN发送
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
u8 Can_Send_Msg(uint32_t std,u8* msg,u8 len)
{
    u16 i=0;
    u8 mbox;
    CanTxMsg TxMessage;
    TxMessage.StdId=std;			// 标准标识符
    TxMessage.IDE=CAN_Id_Standard; // 标准帧
    TxMessage.RTR=CAN_RTR_Data;		 // 数据帧
    TxMessage.DLC=len;						// 要发送的数据长度
    for(i=0; i<len; i++)
        TxMessage.Data[i]=msg[i];
    mbox= CAN_Transmit(CAN1, &TxMessage);
    return 0;
}


/**
  * @brief   	CAN接受中断
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
 */
void USB_LP_CAN1_RX0_IRQHandler(void)
{   
		CanRxMsg can1_rx_message;	
		if (CAN_GetITStatus(CAN1,CAN_IT_FMP0)!= RESET)
		{
				CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
				CAN_Receive(CAN1, CAN_FIFO0, &can1_rx_message);
				interfaceCanReciveDataHandle(can1_rx_message.StdId,can1_rx_message.Data,can1_rx_message.DLC);

		}
}


