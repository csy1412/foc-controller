#include "adc.h"
#include "foc.h"
#include "encoder.h"
#include "interface.h"
/******************************************************************************/		

u32 ADC1_DMA_BUFF[5];

/**
  * @brief    ADC采集初始化
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void CURRENT_SENSOR_Init(void)
{
	ADC_InitTypeDef ADC_InitStructure; 
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |RCC_APB2Periph_ADC1,ENABLE);
	
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);   // 72M/6=12MHz,ADC时钟不能超过14M
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	

	ADC_DeInit(ADC1);
	ADC_InitStructure.ADC_Mode = ADC_Mode_RegInjecSimult;	//ADC独立模式
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;	      //单通道模式
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	//单次转换模式
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T4_CC4;	//软件触发转换
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;	//数据右对齐
	ADC_InitStructure.ADC_NbrOfChannel = 0;	                //规则转换的ADC通道数目
	ADC_Init(ADC1,&ADC_InitStructure);     //初始化
	ADC_Cmd(ADC1, ENABLE);                 //使能ADC1
	ADC_DMACmd(ADC1, ENABLE);
	
//	ADC_RegularChannelConfig(ADC1, 3, 1, ADC_SampleTime_13Cycles5);  //ADC1,ADC通道,顺序值,采样周期	  	
//	ADC_RegularChannelConfig(ADC1, 4, 2, ADC_SampleTime_13Cycles5);  //ADC1,ADC通道,顺序值,采样周期	  	
	
	ADC_InjectedChannelConfig(ADC1, ADC_Channel_3, 0, ADC_SampleTime_28Cycles5);
	ADC_InjectedChannelConfig(ADC1, ADC_Channel_4, 1, ADC_SampleTime_28Cycles5);
	
	ADC_InjectedSequencerLengthConfig(ADC1,2);

	ADC_ExternalTrigInjectedConvConfig(ADC1,ADC_ExternalTrigInjecConv_T4_TRGO);
	ADC_ExternalTrigInjectedConvCmd(ADC1, ENABLE);
	ADC_ExternalTrigConvCmd(ADC1,ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = ADC1_2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
	
	ADC_ITConfig(ADC1,ADC_IT_JEOC,ENABLE);
	ADC_ClearITPendingBit(ADC1,ADC_IT_JEOC);
	
	
	ADC_ResetCalibration(ADC1);	                //开启复位校准 
	while(ADC_GetResetCalibrationStatus(ADC1));	//等待复位校准结束
	ADC_StartCalibration(ADC1);	                //开启校准
	while(ADC_GetCalibrationStatus(ADC1));	    //等待校准结束
	
	ADC_SoftwareStartInjectedConvCmd(ADC1, ENABLE);
	
}


/******************************************************************************/	
unsigned short analogRead(unsigned char ch)   
{
	ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_13Cycles5);  //ADC1,ADC通道,顺序值,采样周期	  			    
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);		      //使能软件触发
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));  //等待转换结束
	return ADC_GetConversionValue(ADC1);	          //返回最近一次的转换结果
}
/******************************************************************************/	
// function reading an ADC value and returning the read voltage
//电压读取
float readADCVoltage(unsigned char ch)
{
  unsigned short raw_adc = analogRead(ch);
  return (float)raw_adc*3.3/4096;
}
/******************************************************************************/	


/**
  * @brief    ADC DMA配置
  * @author   SiYu Chen
  * @param[in]  
  * @retval       
  */


void adcDMAConfig(void)
{

	DMA_InitTypeDef DMA_InitStructure;
	
 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);	//使能DMA传输
	
  DMA_DeInit(DMA1_Channel1);   //将DMA的通道1寄存器重设为缺省值

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&ADC1->JDR1);  //DMA外设基地址
	DMA_InitStructure.DMA_MemoryBaseAddr =(uint32_t)ADC1_DMA_BUFF;  //DMA内存基地址
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;  //外设到内存
	DMA_InitStructure.DMA_BufferSize = 2;  //DMA通道的DMA缓存的大小
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;  //外设地址寄存器不变
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;  //内存地址寄存器递增
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;  //数据宽度为8位
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word; //数据宽度为8位
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;  //工作在正常模式
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium; //DMA通道 x拥有中优先级 
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  //DMA通道x没有设置为内存到内存传输
	DMA_Init(DMA1_Channel1, &DMA_InitStructure);  //根据DMA_InitStruct中指定的参数初始化DMA的通道USART1_Tx_DMA_Channel所标识的寄存器
	DMA_Cmd(DMA1_Channel1,ENABLE);
}



/**
  * @brief   	ADC触发中断
  * @author   SiYu Chen
  * @param[in] 
  * @retval       
  */
void ADC1_2_IRQHandler(void)
{
	if(ADC_GetITStatus(ADC1,ADC_IT_JEOC)!=RESET)
	{
			ADC_ClearITPendingBit(ADC1,ADC_IT_JEOC); //清除注入通道

			interfaceMotorLoop(); //电机FOC循环
	}

}




