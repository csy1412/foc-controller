#ifndef __ADC_H
#define __ADC_H	


#include "stm32f10x.h"
#include "stm32f10x_dma.h"
#include "encoder.h"








/******************************************************************************/	
void CURRENT_SENSOR_Init(void);
unsigned short analogRead(unsigned char ch);
float readADCVoltage(unsigned char ch);
void adcDMAConfig(void);
/******************************************************************************/	
#endif 
