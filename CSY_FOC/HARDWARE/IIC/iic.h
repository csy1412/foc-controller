
#ifndef STM32_IIC_H
#define STM32_IIC_H

#include "stm32f10x.h"


#define  AS5600_Address  0x36
#define  RAW_Angle_Hi    0x0C
#define  AS5600_CPR      4096  //12λ������

void SENSOR_I2C_Init(void);
unsigned short I2C_getRawCount(I2C_TypeDef* I2Cx);


#endif

