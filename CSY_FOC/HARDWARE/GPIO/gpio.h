#ifndef __GPIO_H
#define __GPIO_H	 
#include "sys.h"

void GPIO_ENABLE_Init(void);

#define M1_Enable    GPIO_SetBits(GPIOC,GPIO_Pin_14);          //高电平使能
#define M1_Disable   GPIO_ResetBits(GPIOC,GPIO_Pin_14);        //低电平失能


#endif
