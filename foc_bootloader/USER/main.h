#ifndef __MAIN_H
#define __MAIN_H

#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"
#include "stmflash.h" 
#include "math.h"
#include "app.h"
#include "timer.h"
#include "iap.h"
#include "led_function.h"

typedef enum
{
	run_app=0,
	receive_data,
	receive_ok,
}process_update_e;


typedef struct
{
	int sum_size;
	int receive_size;
	
}system_update_t;


#endif