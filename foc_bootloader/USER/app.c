#include "main.h"
#include "protool.h"

void BspInit(void)
{
	delay_init();	    	 //延时函数初始化	  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	protocol_init();
	uart_init(115200);	 //串口初始化为115200
	LED_Init();			     //LED端口初始化
	KEY_Init();          //初始化与按键连接的硬件接口
	TIM4_1ms_Init();
}



void ParamInit(void)
{
	ledStateSwitch(led_init);
	DEBUG("bootloader version %s (2022-5-13) \r\n",APPLICATION_VERSION);
	


}