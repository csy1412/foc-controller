#include "main.h"

extern _Bool time_1ms;
extern _Bool time_10ms;
extern _Bool time_100ms;
extern _Bool time_500ms;
extern _Bool time_1000ms;

process_update_e process_update; 
system_update_t system_update;

int main(void)
{		

	RCC_ClocksTypeDef  get_rcc_clock; 
RCC_GetClocksFreq(&get_rcc_clock);
	BspInit();
	ParamInit();
	static int t=0;
	while(1)
	{	
		if(time_100ms) //初始化等待1S
		{
			time_100ms=0;	
			t++;
			if(t>=5&&process_update==run_app)
			{
				if(((*(vu32*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
					{	 
						iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
					}
			}
		}
	}	 
}

